FROM python:3.7

RUN mkdir /app
COPY . /app
WORKDIR /app

RUN apt-get update && \
  apt-get install -y \
      git \
      python3-dev \
      libffi-dev \
      libssl-dev \
      libxml2-dev \
      zlib1g-dev \
      libpq-dev \
      cmake \
      openssh-server \
      openssh-client


# 1. Create the SSH directory.
# 2. Populate the private key file.
# 3. Set the required permissions.
# 4. Add github to our list of known hosts for ssh.
ARG SSH_KEY
RUN mkdir -p /root/.ssh/ && \
    echo "$SSH_KEY" > /root/.ssh/id_rsa && \
    chmod -R 600 /root/.ssh/ && \
    ssh-keyscan -t rsa -T 60 bitbucket.org >> ~/.ssh/known_hosts

RUN pip install -r requirements.txt

EXPOSE 80
