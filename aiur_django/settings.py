"""
Django settings for aiur_django project.

Generated by 'django-admin startproject' using Django 2.2.3.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os
import datetime

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '+kd5*m-zq=qtkrh#sqdir1cj%!pca%a-bgqzx+6yc_x*8vcph8'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['.projectaiur.com']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
    'aiur',
    'rest_framework',
    'django_filters',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'aiur_django.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'aiur_django.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

# set db credentials in settings_local.py
DATABASES = {
    'default': {
        'ENGINE': 'iris_django_postgrespool',
        'NAME': 'dbname',
        'USER': 'user',
        'PASSWORD': 'pass',
        'HOST':  'host',
        'PORT': '5432'
    }
}

AUTH_USER_MODEL = 'aiur.User'

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

CORS_ORIGIN_REGEX_WHITELIST = ('^(http|https)?(://)?([\w\.-]+)?projectaiur\.com$')
CORS_ALLOW_CREDENTIALS = True


REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'iris_auth.permissions.IsAuthenticatedUser',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'aiur.authentication.UserJWTAuthentication',
        'iris_auth.authentication.ClientJWTAuthenticationBase',
    ),
    'DEFAULT_PAGINATION_CLASS': 'aiur.pagination.ClientBasedPageNumberPagination',
}


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'app.logger': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
        },
    },
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'


EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'webadmin@iris.ai'
EMAIL_HOST_PASSWORD = 'secret'
EMAIL_PORT = '587'
EMAIL_USE_TLS = True


JWT_AUTH = {
    'JWT_SECRET_KEY': 'key',
    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=900),
    'CLIENT_JWT_EXPIRATION_DELTA': datetime.timedelta(days=1),
    'REFRESH_JWT_EXPIRATION_DELTA': datetime.timedelta(days=5),
    'JWT_AUTH_COOKIE': 'dev_jwt',
    'LOGGER_NAME': 'app.logger',
    'CLIENT_MODEL': 'aiur.Client',
}

IRIS_CLIENT_URL = 'https://projectaiur.com'
DATA_PATH = '/home/ubuntu/dataset/'
MODELS_PATH = '/home/ubuntu/ml_models/'
GRAPH_FILENAME = 'graph.json'
GRAPH_FILEPATH = os.path.join(DATA_PATH, GRAPH_FILENAME)


try:
    # Set secrets and local settings in settings_local.py
    # as is not tracked from version control.
    from .settings_local import * # NOQA
except ImportError as e:
    print('Could not import settings_local!', e)
    pass
