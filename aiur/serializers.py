from django.contrib.auth import get_user_model
from rest_framework import serializers

import json

from aiur.models import Document, Tree, User, Expertise


class JSONStoredField(serializers.Field):
    def to_representation(self, value):
        return json.loads(value)

    def to_internal_value(self, data):
        return json.dumps(data)


class OwnerField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        user = self.context['request'].user
        queryset = User.objects.filter(id=user.id)
        return queryset


class UserSerializer(serializers.ModelSerializer):
    expertises = serializers.SlugRelatedField(many=True, read_only=True, slug_field='name')

    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'first_name', 'position_in_company', 'expertises', 'seniority')

    def to_internal_value(self, data):
        if 'position_in_company' in data:
            data['position_in_company'] = data['position_in_company'].lower()
        if 'seniority' in data:
            data['seniority'] = data['seniority'].lower()
        return super(UserSerializer, self).to_internal_value(data)


class OwnerSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('id', 'first_name')
        read_only_fields = ('id', 'first_name')


class TreeSerializer(serializers.ModelSerializer):
    dependency_tree = JSONStoredField()
    owner_id = OwnerField(source='owner')
    owner = OwnerSerializer(read_only=True)
    document_id = serializers.PrimaryKeyRelatedField(queryset=Document.objects.all(), source='document')

    class Meta:
        model = Tree
        fields = ('id', 'type', 'document_id', 'status', 'owner', 'owner_id', 'dependency_tree', 'created_at',
                  'modified_at')


class DocumentSerializer(serializers.ModelSerializer):
    trees = TreeSerializer(many=True, read_only=True)
    owner_id = OwnerField(source='owner')
    owner = OwnerSerializer(read_only=True)
    tags = serializers.SlugRelatedField(many=True, read_only=True, slug_field='name')

    class Meta:
        model = Document
        fields = ('id', 'document_identifier', 'title', 'abstract', 'authors', 'year', 'tags', 'status',
                  'fulltext_link', 'owner', 'owner_id', 'short_name', 'trees', 'created_at', 'modified_at')


class ExpertiseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Expertise
        fields = ('id', 'name')
