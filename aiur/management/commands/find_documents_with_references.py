import time
from ctypes import c_bool

from django.core.management import BaseCommand

from multiprocessing import JoinableQueue, Process, Value

from aiur.nosql_client.articles_service import CoreBackupArticles
from aiur.tools.reference_extractor import build_document_with_references, get_binary_pdf

NUM_WORKERS = 5
TASK_QUEUE_SIZE = NUM_WORKERS * 1000
JOINABLE_QUEUE_MAX_SIZE = 100000
QUERY = 'doc.title:lstm AND doc.repositories.id:144'


class Command(BaseCommand):
    help = 'Find documents in ES which are related (have references) to those in the database'

    def handle(self, *args, **options):

        tasks = JoinableQueue(maxsize=TASK_QUEUE_SIZE)
        related_papers_urls = JoinableQueue(maxsize=JOINABLE_QUEUE_MAX_SIZE)
        all_jobs_in = Value(c_bool, False)

        processes = [Process(target=get_article_references, args=(tasks, all_jobs_in, related_papers_urls))
                     for _ in range(NUM_WORKERS)]

        processes.append(Process(target=writer_process, args=(related_papers_urls,)))

        for p in processes:
            p.start()
        print('Processes started')

        try:
            articles_service = CoreBackupArticles()
            for page in range(1, 101):
                response = articles_service.native_search(query=QUERY, pagesize=100, page=page)

                for article in response['hits']['hits']:
                    tasks.put(article, block=True)
        finally:
            tasks.join()
            all_jobs_in.value = True
            time.sleep(10)
            for p in processes:
                if p.is_alive():
                    p.terminate()
                    print('Terminating process')


def get_article_references(tasks, stop_condition, related_papers_urls):
    while True:
        article = tasks.get()
        try:
            full_text_identifiers = article['_source']['doc'].get('fulltextIdentifier')
            if not full_text_identifiers:
                urls = article['_source']['doc']['fulltextUrls']
                full_text_identifiers = urls[-1]
            full_text_identifiers = full_text_identifiers.replace('abs', 'pdf')
            full_text_identifiers += '.pdf'
            print(full_text_identifiers)

            article_from_graph = build_document_with_references(get_binary_pdf(full_text_identifiers))
            if article_from_graph and len(article_from_graph.children) > 0:
                article_dict = article_from_graph.to_dict()
                print(article_dict['fulltext_link'])
                related_papers_urls.put(article_dict['fulltext_link'])
        except Exception:
            print('There was exception in execution for {}'.format(full_text_identifiers))

        tasks.task_done()

        if tasks.empty() and stop_condition.value:
            break


def writer_process(message_update_queue):
    with open('document_with_references.txt', 'w') as out:
        while True:
            message = message_update_queue.get()
            print(message)
            if message is None:
                break
            out.write('{} \n'.format(message))
            out.flush()

    print('END!!!')
