from django.core.management.base import BaseCommand

from django.conf import settings

from aiur.models import Document
from aiur.models import Expertise
from aiur.tools.document_classifier import DocumentAnalyzer

import os


class Command(BaseCommand):
    help = 'Add category to documents'

    def handle(self, *args, **kwargs):
        w2v_filename = os.path.join(settings.MODELS_PATH, 'abstracts_w2v.mdl')
        categories = Expertise.objects.all().values_list('name', flat=True)
        document_category_analyzer = DocumentAnalyzer(w2v_filename=w2v_filename)

        non_categorized_documents = Document.objects.filter(tags=None)

        for doc in non_categorized_documents:
            category, _ = document_category_analyzer.classify_doc_into_categories(doc.title, doc.abstract, categories)
            try:
                tag = Expertise.objects.get(name=category)
                doc.tags.add(tag)
                print('Tagged document with id={doc_id} with category {tag}'.format(doc_id=doc.pk, tag=tag.name))
            except Expertise.DoesNotExist:
                print('Expertise {} not found'.format(category))
