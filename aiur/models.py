from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import gettext_lazy as _

from iris_auth.abstract_models import GenericModel, PrivateModel


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a user with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a superuser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class Expertise(GenericModel):
    name = models.CharField(max_length=255)


class User(AbstractUser, GenericModel):
    JUNIOR = 'junior'
    SENIOR = 'senior'
    SENIORITY_CHOICES = [
        (JUNIOR, 'Junior'),
        (SENIOR, 'Senior'),
    ]
    RESEARCHER = 'researcher'
    INTERN = 'intern'
    OTHER = 'other'
    RESEARCHER = 'researcher'
    POSITION_CHOICES = [
        (RESEARCHER, 'Researcher'),
        (INTERN, 'Intern'),
        (OTHER, 'Other'),
    ]

    username = None
    email = models.EmailField(_('email'), unique=True)

    position_in_company = models.CharField(max_length=255, choices=POSITION_CHOICES, default=RESEARCHER)
    expertises = models.ManyToManyField(Expertise)
    seniority = models.CharField(max_length=255, choices=SENIORITY_CHOICES, default=SENIOR)

    failed_login_attempts = models.IntegerField(default=0)
    last_failed_login_attempt = models.DateTimeField(default=None, blank=True, null=True)

    USERNAME_FIELD = 'email'  # use email as username
    REQUIRED_FIELDS = []
    objects = UserManager()


class Client(GenericModel, PrivateModel):
    id = models.AutoField(primary_key=True)
    api_key = models.CharField(max_length=50, unique=True)
    info = models.CharField(max_length=2048)


class Document(GenericModel):
    SUBMITTED = 'submitted'
    IN_PROGRESS = 'in-progress'
    UNDER_APPROVAL = 'under-approval'
    INTEGRATED = 'integrated'
    STATUS_CHOICES = [
        (SUBMITTED, SUBMITTED),
        (IN_PROGRESS, IN_PROGRESS),
        (UNDER_APPROVAL, UNDER_APPROVAL),
        (INTEGRATED, INTEGRATED),
    ]

    id = models.AutoField(primary_key=True)
    document_identifier = models.CharField(max_length=255, unique=True)
    title = models.CharField(max_length=2048)
    abstract = models.TextField()
    authors = models.TextField(blank=True, null=True)
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    tags = models.ManyToManyField(Expertise)
    status = models.CharField(max_length=255, choices=STATUS_CHOICES, default=SUBMITTED)
    year = models.IntegerField()
    journals = models.TextField(blank=True, null=True)
    fulltext_link = models.CharField(max_length=4096)
    download_url = models.CharField(max_length=4096, null=True)
    fingerprint = models.TextField(blank=True, null=True)
    short_name = models.CharField(max_length=255, null=True)

    @staticmethod
    def create_document_from_paper_abstract(paper_abstract, user):
        doc = Document()
        doc.document_identifier = paper_abstract.identifier
        doc.title = paper_abstract.title
        doc.abstract = paper_abstract.abstract
        doc.year = paper_abstract.year
        doc.owner = user
        doc.journals = [paper_abstract.journal]
        doc.fulltext_link = paper_abstract.fulltext_link
        doc.download_url = paper_abstract.download_url
        doc.fingerprint = paper_abstract.fingerprint
        doc.short_name = paper_abstract.name

        return doc


class Tree(GenericModel):
    SUBMITTED = 'submitted'
    IN_PROGRESS = 'in-progress'
    APPROVED = 'approved'
    STATUS_CHOICES = [
        (SUBMITTED, SUBMITTED),
        (IN_PROGRESS, IN_PROGRESS),
        (APPROVED, APPROVED),
    ]
    SUBMISSION = 'submission'
    REVIEW = 'review'
    TYPE_CHOICES = [
        (SUBMISSION, SUBMISSION),
        (REVIEW, REVIEW),
    ]

    id = models.AutoField(primary_key=True)
    document = models.ForeignKey(Document, related_name='trees', on_delete=models.CASCADE)
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    dependency_tree = models.TextField()
    status = models.CharField(max_length=255, choices=STATUS_CHOICES, default=SUBMITTED)
    type = models.CharField(max_length=255, choices=TYPE_CHOICES, default=SUBMISSION)

    class Meta:
        unique_together = ("owner", "document")

    def __repr__(self):
        return 'Tree(id={id}, document={doc_id}, owner={owner_id}, status={status}, type={type})'.format(
                id=self.id,
                doc_id=self.document.id,
                owner_id=self.owner.id,
                status=self.status,
                type=self.type,
            )
