from dry_rest_permissions.generics import DRYPermissionFiltersBase
from iris_auth.permissions import is_registered_user

__author__ = 'Viktor Botev <victor@iris.ai>'
__copyright__ = "Copyright (C) 2017 IRIS.AI"
__version__ = "0.1"


class PerUserFilterBackend(DRYPermissionFiltersBase):

    def filter_list_queryset(self, request, queryset, view):
        """
        Limits all list requests to only be seen by the owners.
        """
        if is_registered_user(request.user):
            return queryset.filter(owner=request.user)
        else:
            return queryset.none()
