from urllib3.exceptions import ReadTimeoutError

from aiur.nosql_client.es_client import ElasticSearchClient
from aiur.tools.url_utils import get_valid_url, order_for_full_text_link, fix_fulltext_url


COUCHBASE_DOCUMENT = 'couchbaseDocument'

BUCKET_CORE_AC_UK = 'core_ac_uk'
BUCKET_CORE_AC_UK_BACKUP = 'core_ac_uk_backup'

FINGERPRINT_ONLY_PAGESIZE = 500
PAGESIZE = 30

MIN_PAGESIZE = 3
MAX_PAGESIZE = 10000

MIN_WORD_COUNT = 100

DOI_PREFIX = 'doi:'
OAI_PREFIX = 'oai:'


def create_full_query(query_dict):
    '''
    constructs a valid lucene query string from query terms in `query_dict`
    :param query_dict:      [dict] field to query value. The query will be `doc.{field}:"value"`
    :return: query string:  [str]
    '''
    query_terms = ['doc.{}:"{}"'.format(field, value) for field, value in query_dict.items()]
    generic_query = '(' + ' AND '.join(query_terms) + ')'
    # the article should not be marked as duplicate, so the value of doc.duplicate should be 'false' or
    # the field should not exist
    is_not_duplicate_query = '((doc.duplicate:false) OR (NOT _exists_:doc.duplicate))'
    # the article should be an English article and should contain fingerprint and should not have
    # an abstract with less then 99 words
    is_valid_article = '(doc.language.code:en AND doc.abstract_word_count:>{})'.format(MIN_WORD_COUNT - 1)
    query = ' AND '.join([generic_query, is_not_duplicate_query, is_valid_article])
    return query


class CoreBackupArticles:
    TITLE = 'title'
    YEAR = 'year'
    FINGERPRINT = 'document_fingerprint'
    ABSTRACT = 'description'
    DOI = 'doi'
    OAI = 'oai'

    FULLTEXTID = 'fulltextIdentifier'
    FULLTEXTURLS = 'fulltextUrls'
    FULLTEXT_LINK = 'fulltext_link'
    DOWNLOAD_URL = 'downloadUrl'

    IDENTIFIERS_LIST = 'identifiers'
    IDENTIFIER = 'identifier'
    CITATIONS = 'citations'

    def __init__(self):
        self.es_client = ElasticSearchClient()
        self.bucket_name = BUCKET_CORE_AC_UK_BACKUP
        self.doc_type = COUCHBASE_DOCUMENT

    def es_search_with_retry(self, query, pagesize, page, timeout='25', method='GET'):
        search_params = dict(index=self.bucket_name, rest_total_hits_as_int='true')
        if method == 'GET':
            search_params['params'] = {'q': query,
                                       'size': pagesize,
                                       'from': (page - 1) * pagesize}
        elif method == 'POST':
            search_params.update(dict(body={'query': {'query_string': {'query': query}}},
                                      size=pagesize,
                                      timeout=str(timeout) + 's',
                                      from_=(page - 1) * pagesize))
        else:
            raise NotImplementedError('Such method is not supported yet!')

        try:
            return self.es_client.search(**search_params)
        except ReadTimeoutError as e:
            print(e)
            print('ElasticSearch timed out, retrying')
            return self.es_client.search(**search_params)

    def search(self, query_dict, page=1, pagesize=PAGESIZE):
        '''
            query must be a valid lucene query
            sort must be field:[asc|desc]
        '''
        try:
            int(pagesize)
        except ValueError:
            raise ValueError('pagesize must be integer')

        if pagesize > MAX_PAGESIZE or pagesize < MIN_PAGESIZE:
            raise ValueError('pagesize must be between %s and %s', (MIN_PAGESIZE, MAX_PAGESIZE))

        full_query = create_full_query(query_dict)
        response = self.native_search(full_query, page, pagesize)

        return response

    def native_search(self, query, page=1, pagesize=PAGESIZE, method='GET'):
        '''
        This method returns a list of len equal to pagesize.
        '''
        try:
            int(pagesize)
        except ValueError:
            raise ValueError('pagesize must be integer')

        if pagesize > MAX_PAGESIZE or pagesize < MIN_PAGESIZE:
            raise ValueError('pagesize must be between %d and %d' % (MIN_PAGESIZE, MAX_PAGESIZE))

        if pagesize*page > 10000:
            raise ValueError('cannot use search for more than 10000 results, use bulk_search instead')

        response = self.es_search_with_retry(query, pagesize, page, method=method)
        return response

    @classmethod
    def prepare_article(cls, article, validate_links=True):
        article_output = {key: article.get(key) for key in
                          [cls.TITLE, cls.YEAR, cls.ABSTRACT, cls.FINGERPRINT]}

        prefix = ''
        identifier = None
        if cls.DOI in article:
            prefix = DOI_PREFIX
            identifier = article[cls.DOI]
        elif cls.OAI in article:
            prefix = OAI_PREFIX
            identifier = article[cls.OAI]
        elif cls.IDENTIFIERS_LIST in article:
            identifier = article[cls.IDENTIFIERS_LIST][0]

        if identifier and not identifier.startswith(prefix):
            identifier = prefix + identifier

        article_output[cls.IDENTIFIER] = identifier
        article_output[cls.FULLTEXT_LINK] = cls.choose_url_for_article(article, order_for_full_text_link,
                                                                       validate_existence=validate_links)
        # check for both None and empty string entries of crucial entries
        is_valid = all(article_output.values())

        # fill in non-crucial entries
        article_output[cls.CITATIONS] = article.get(cls.CITATIONS)
        article_output[cls.DOWNLOAD_URL] = cls.find_download_url(article, validate_existence=validate_links)
        return is_valid, article_output

    @classmethod
    def find_download_url(cls, article, validate_existence=True):
        urls = []
        download_url = article.get(cls.DOWNLOAD_URL)
        if download_url:
            urls.append(fix_fulltext_url(download_url))

        urls += [fix_fulltext_url(url) for url in article.get(cls.FULLTEXTURLS, []) if url.endswith('.pdf')]
        if not urls:
            return None

        url = get_valid_url(urls) if validate_existence else urls[0]
        return url

    @classmethod
    def choose_url_for_article(cls, article, ordering_fn, use_identifier=True, validate_existence=True):
        all_urls = [fix_fulltext_url(url) for url in article[cls.FULLTEXTURLS]]
        if use_identifier and cls.FULLTEXTID in article:
            all_urls.insert(0, article[cls.FULLTEXTID])
        if not all_urls:
            return None
        all_urls.sort(key=ordering_fn, reverse=True)

        url = get_valid_url(all_urls) if validate_existence else all_urls[0]
        return url
