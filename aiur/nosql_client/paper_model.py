from aiur.nosql_client.articles_service import CoreBackupArticles


class PaperAbstract:

    def __init__(self, title, year, identifier=None, journal=None, fulltext_link=None, download_url=None, abstract=None,
                 fingerprint=None, tag=None):
        self.title = title
        self.year = year
        self.identifier = identifier

        self.journal = journal  # this is not used for now
        self.fulltext_link = fulltext_link
        self.download_url = download_url
        self.abstract = abstract
        self.fingerprint = fingerprint
        self.tag = tag

        self.children = []
        self.name = None
        self.update_name()

        self.articles_service = CoreBackupArticles()

    def __str__(self):
        repr_parts = []
        for name, value in sorted(vars(self).items()):
            if not value or name == 'articles_service':
                continue
            repr_parts.append('{}: {}'.format(name, value))
        return '\n'.join(repr_parts)

    def update_name(self):
        if self.title:
            short_title = '_'.join(self.title.lower().split()[:5])
            if self.identifier:
                short_title += '_' + self.identifier
            elif self.year:
                short_title += '_{}'.format(self.year)
            self.name = short_title

    def update_from_article_dict(self, article):
        '''
        update from a dict resulting from CoreBackupArticles.prepare_article
        :param article: [dict]
        '''
        self.title = article.get(CoreBackupArticles.TITLE, self.title)
        self.year = article.get(CoreBackupArticles.YEAR, self.year)
        self.abstract = article.get(CoreBackupArticles.ABSTRACT, self.abstract)
        self.fingerprint = article.get(CoreBackupArticles.FINGERPRINT, self.fingerprint)

        self.identifier = article.get(CoreBackupArticles.IDENTIFIER, self.identifier)
        self.fulltext_link = article.get(CoreBackupArticles.FULLTEXT_LINK, self.fulltext_link)
        self.download_url = article.get(CoreBackupArticles.DOWNLOAD_URL, self.download_url)

        self.update_name()

    def to_dict(self, depth=0, depth_explicit=False):
        paper_dict = {
            'title': self.title,
            'year': self.year,
            'identifier': self.identifier,

            'journal': self.journal,
            'fulltext_link': self.fulltext_link,
            'download_url': self.download_url,
            'abstract': self.abstract,
            'fingerprint': self.fingerprint,
            'tag': self.tag,

            'name': self.name,
            'metadata': self.metadata if hasattr(self, 'metadata') and self.metadata is not None else {'type': 'original'}
        }
        if depth == 0 and depth_explicit:
            paper_dict['children'] = []
        elif depth == 0:
            paper_dict['children'] = [c.identifier if hasattr(c, 'identifier') else c for c in self.children]
        else:
            paper_dict['children'] = [c.to_dict(depth=depth-1, depth_explicit=depth_explicit) for c in self.children]
        return paper_dict

    @classmethod
    def from_dict(cls, paper_dict):
        paper = cls(
            title=paper_dict['title'],
            year=paper_dict['year'],
            identifier=paper_dict['identifier'],
            journal=paper_dict.get('journal'),
            fulltext_link=paper_dict.get('fulltext_link'),
            download_url=paper_dict.get('download_url'),
            abstract=paper_dict.get('abstract'),
            fingerprint=paper_dict.get('fingerprint'),
            tag=paper_dict.get('tag')
        )
        paper.children = paper_dict.get('children', [])
        paper.metadata = paper_dict.get('metadata')

        return paper
