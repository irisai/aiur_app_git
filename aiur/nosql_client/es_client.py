from elasticsearch.client import Elasticsearch

from aiur.nosql_client.es_credentials import env_map


class ElasticSearchClient:
    def __init__(self):
        es_user = env_map['ES_USER']
        es_password = env_map['ES_PASS']
        es_host = env_map['ES_HOST']
        es_port = env_map['ES_PORT']

        self.es_client = Elasticsearch([{
            'host': 'https://{}:{}@{}:{}/'.format(es_user, es_password, es_host, es_port)
        }], timeout=30, max_retries=10, retry_on_timeout=True)

    def search(self, **kwargs):
        return self.es_client.search(**kwargs)
