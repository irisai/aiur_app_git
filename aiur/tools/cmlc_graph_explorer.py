from collections import defaultdict

from aiur.tools.build_dependency_graph import DependencyGraphBuilder

# GRAPH_FILEPATH = "/mnt/Data/Python/ledger_app/backend/tools/data_folder/graph.json"
GRAPH_FILEPATH = r'C:\Users\vici4a\Downloads\LEDGER-dataset-444\dataset_444_flattened\dataset_444_flattened\graph.json'

NAMES = "n"
REFS = "r"
QUIT = "q"
COMPONENTS = "c"


def depth_first_search(graph, node, visited=None):
    if not visited:
        visited = set()
    visited.add(node)
    for neighbor in graph[node] - visited:
        depth_first_search(graph, neighbor, visited)
    return visited


def print_refs(paper, max_levels, level=0):
    print(4 * level * " " + paper.name)
    if level < max_levels:
        for ref in paper.children:
            print_refs(ref, max_levels, level + 1)


def get_undirected_graph(id_to_paper):
    graph = defaultdict(set)
    for p_id, paper in id_to_paper.items():
        for ref in paper.children:
            r_id = ref.identifier
            graph[p_id].add(r_id)
            graph[r_id].add(p_id)
    return graph


def main():
    builder = DependencyGraphBuilder.load(GRAPH_FILEPATH)
    id_to_paper = builder.id_to_paper

    id_to_name = {p_id: paper.name for p_id, paper in id_to_paper.items()}
    name_to_id = {name: p_id for p_id, name in id_to_name.items()}
    u_graph = None

    max_width_names = max([len(name) for name in name_to_id]) + 4
    max_width_ids = max([len(p_id) for p_id in id_to_name]) + 4
    fmts_str = "{{:>3}}: {{:<{}}}: {{:<{}}}: {{}} refs".format(max_width_ids, max_width_names)

    run = True
    while run:
        print(80 * "-")
        print("choose action:")
        print("print names and ids ['n']")
        print("print references of name/id ['r']")
        print("print connected components ['c']")
        print("quit ['q']")
        action = input("action: ")

        if action[0] == NAMES:
            print()
            for i, (p_id, paper) in enumerate(id_to_paper.items()):
                print(fmts_str.format(i, p_id, paper.name, len(paper.children)))
        elif action[0] == REFS:
            key = input("name/id ['a' for all]: ")
            levels = int(input("levels: "))

            papers_to_display = []
            if key == 'a':
                papers_to_display = id_to_paper.values()
            else:
                p_id = key if key in id_to_paper else name_to_id.get(key)
                paper = id_to_paper.get(p_id)
                if paper:
                    papers_to_display = [paper]
                else:
                    print("{} not found".format(p_id))
            for paper in papers_to_display:
                print()
                print_refs(paper, levels)
        elif action[0] == COMPONENTS:
            if not u_graph:
                u_graph = get_undirected_graph(id_to_paper)
            p_ids = set(id_to_paper.keys())

            components = []
            while p_ids:
                p_id = p_ids.pop()
                component = depth_first_search(u_graph, p_id)
                components.append(component)
                p_ids = p_ids - component

            print()
            print("{} connected components:".format(len(components)))
            for i, c in enumerate(components):
                print()
                print("{}:".format(i))
                for k, p_id in enumerate(c):
                    print(4 * " " + "{}: ".format(k) + id_to_name[p_id])

        elif action[0] == QUIT:
            run = False
        else:
            print("not recognized")
    print("done")


if __name__ == '__main__':
    main()
