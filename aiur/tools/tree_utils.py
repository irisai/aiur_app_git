def dfs_add_ord_ids(dependency_tree, num=1):
    if 'ord_id' not in dependency_tree:
        dependency_tree['ord_id'] = num

    for child in dependency_tree['children']:
        num += 1
        num = dfs_add_ord_ids(child, num)

    return num


def dfs_find_by_ord_id(tree, ord_id):
    ''' Search node by ord_id id and return it or return None if not found. '''
    if tree['ord_id'] == ord_id:
        return tree

    for child in tree['children']:
        el = dfs_find_by_ord_id(child, ord_id)
        if el and el['ord_id'] == ord_id:
            return el
