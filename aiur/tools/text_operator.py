import string
import re

PUNCTUATION_NO_DASH_NO_SHARP_NO_AMPERSAND = "".join([symbol for symbol in string.punctuation if symbol not in "-#&"])


def tokenize_text(text, split_chars=PUNCTUATION_NO_DASH_NO_SHARP_NO_AMPERSAND):
    regex_for_split = re.compile(r"[^" + string.whitespace + split_chars + r"]+")
    tokens = re.findall(regex_for_split, text)
    return tokens
