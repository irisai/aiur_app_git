import os
import requests
import simplejson

from django.conf import settings

from aiur.models import Expertise
from aiur.nosql_client.articles_service import CoreBackupArticles
from aiur.tools.document_classifier import DocumentAnalyzer
from aiur.tools.build_dependency_graph import DependencyGraphBuilder
from aiur.nosql_client.paper_model import PaperAbstract

__author__ = 'Viktor Botev <victor@iris.ai>'
__copyright__ = "Copyright (C) 2017 IRIS.AI"
__version__ = "0.1"

W2V_MODEL_PATH = os.path.join(settings.MODELS_PATH, 'abstracts_w2v.mdl')
document_analyzer = DocumentAnalyzer(w2v_filename=W2V_MODEL_PATH)
graph_builder = DependencyGraphBuilder.load(settings.GRAPH_FILEPATH)


def build_document_with_references(binary_pdf):
    categories = Expertise.objects.all().values_list('name', flat=True)

    headers = {"Content-type": "application/pdf"}
    response = requests.post('http://localhost:8080/v1', files={'file': binary_pdf}, headers=headers)

    processed_file = simplejson.loads(response.content)
    reference_list = processed_file['references']
    output_article = graph_builder.create_article_from_ES(processed_file['title'], processed_file['year'])
    output_article = graph_builder.get_paper_node_from_article_dict(output_article)
    existing_references = []
    for reference in reference_list:
        prepared_article = graph_builder.create_article_from_ES(reference['title'], reference['year'])
        ref_paper = graph_builder.id_to_paper.get(prepared_article[CoreBackupArticles.IDENTIFIER])
        if ref_paper:
            existing_references.append(ref_paper)
    output_article.children = existing_references

    tag, _ = document_analyzer.classify_doc_into_categories(output_article.title, output_article.abstract, categories)
    output_article.tag = tag

    return output_article


def get_binary_pdf(url):
    headers = {"Content-type": "application/pdf"}
    response = requests.get(url=url, headers=headers)
    return response.content


def add_dependency_tree_to_graph(dependency_tree):
    paper_abstract = PaperAbstract.from_dict(simplejson.loads(dependency_tree))
    graph_builder.add_vertex(paper_abstract)
    graph_builder.save()


if __name__ == '__main__':
    article = build_document_with_references(get_binary_pdf('https://arxiv.org/pdf/1707.03631.pdf'))
    dependency_tree = simplejson.dumps(article.to_dict(depth=3, depth_explicit=True))
    print(dependency_tree)
