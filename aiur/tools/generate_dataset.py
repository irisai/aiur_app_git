import os
import json
from pdfx.downloader import download_urls

from aiur.nosql_client.articles_service import CoreBackupArticles

es_service = CoreBackupArticles()


def get_proper_url(reference):
    url = reference['downloadUrl']
    # The downloadUrl is OK if the it points to core (like https://core.ac.uk/download/pdf/53854611.pdf)
    # If the downloadUrl points to arxix.org we have to modify it a bit
    if 'arxiv.org' in url or 'core.ac.uk' in url:
        return url.replace('abs', 'pdf') + '.pdf' if 'arxiv.org' in url else url
    return None


DIR_QUEUE = ['./pdf_files']
LEVEL_OF_DEEPNESS = 2

while LEVEL_OF_DEEPNESS != 0:  # This is not working for NOW!!! It will loop forever!!!
    while len(DIR_QUEUE):
        PDFS_DIR = DIR_QUEUE.pop(0)
        COMMAND = 'java -Xmx6g -jar science-parse-cli-assembly-2.0.3.jar {} -o {}'.format(PDFS_DIR, PDFS_DIR)

        print('RUNNING COMMAND:\n{}'.format(COMMAND))

        os.system(COMMAND)

        for filename in [f for f in os.listdir(PDFS_DIR) if f.endswith('.json')]:
            references = json.load(open('{}/{}'.format(PDFS_DIR, filename), 'r', encoding='UTF-8'))['metadata']['references']
            print('FILE: {}\nFOUND {} REFERENCES'.format(filename, len(references)))
            found_in_our_ES = list()
            for reference in references:
                query = 'doc.title:"{}" AND doc.abstract_word_count:>100'.format(reference['title'].replace('"', ''))
                query_result = es_service.native_search(query, pagesize=50)
                query_result = [res['_source'] for res in query_result['hits']['hits']]
                for item in query_result:
                    es_title = item['doc']['title'].lower()
                    reference_title = reference['title'].lower()
                    es_year = item['doc']['year']
                    reference_year = reference['year']
                    es_authors = set(author.split()[-1].replace('.', '') for author in item['doc']['authors'])
                    reference_authors = set(author.split()[-1].replace('.', '') for author in reference['author'])
                    citations = query_result[0]['doc']['citations']
                    if es_title == reference_title:  # and len(citations):
                        # ???
                        # and es_authors == es_authors
                        # and es_year == reference_year
                        found_in_our_ES.append(item['doc'])
                        break

            pdf_urls = [get_proper_url(reference) for reference in found_in_our_ES if reference['downloadUrl']]
            pdf_urls = [url for url in pdf_urls if url is not None]  # Remove the broken URLs

            message = 'Out of them {} are found in our ElasticSearch, but only {} of them have working "downloadUrl"'

            print(message.format(len(found_in_our_ES), len(pdf_urls)))

            if pdf_urls:
                print('Downloading...')
                new_dir = '{}/{}_children'.format(PDFS_DIR, filename[:-9])
                os.mkdir(new_dir)
                download_urls(pdf_urls, output_directory=new_dir)
                DIR_QUEUE.append(new_dir)

    LEVEL_OF_DEEPNESS -= 1
