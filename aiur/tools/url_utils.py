import requests

CORE_DISPLAY_URL = 'https://core.ac.uk/display'


def check_url_existence(link):
    try:
        resp = requests.head(link)
        if resp.status_code < 400:
            return True
    except Exception:
        print('Error accessing link')
    return False


def get_valid_url(urls):
    for url in urls:
        if check_url_existence(url):
            return url
    return None


def order_for_full_text_link(url):
    '''
    Choose url from array of articles based on
    html or pdf or /core or /search
    Order of article type html, pdf, /core, /search
    Return url
    '''
    return 'pdf' in url, CORE_DISPLAY_URL in url, '/search?' in url


def fix_fulltext_url(url):
    url = url.replace('abs', 'pdf') + '.pdf' if 'arxiv.org' in url else url
    return url
