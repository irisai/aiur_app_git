import numpy
from gensim.models import Word2Vec
from nltk.corpus import stopwords

from aiur.tools.text_operator import tokenize_text

NLTK_CORPUS_STOPWORDS = set(stopwords.words("english"))


class DocumentAnalyzer:
    def __init__(self, w2v_filename=None, w2v_model=None):
        if not w2v_filename and not w2v_model:
            raise ValueError("at least one of `w2v_filename` or `w2v_model` must be given")

        self.w2v_model = w2v_model if isinstance(w2v_model, Word2Vec) else Word2Vec.load(w2v_filename)

    def get_ngram_representation(self, ngram, use_direct_representation=False):
        ngram = ngram.replace(" ", "_")
        if use_direct_representation:
            try:
                w2v_repr_of_ngram = self.w2v_model.wv[ngram]
                return w2v_repr_of_ngram
            except KeyError:
                pass

        constituent_words = ngram.split("_")
        w2v_repr_of_ngram = self.w2v_model.wv[constituent_words].mean(axis=0)

        return w2v_repr_of_ngram

    def classify_doc_into_categories(self, title, abstract, categories):
        """
        computes the w2v similarities between a document and words/phrases describing a categories.

        :param title:                       [str] title of document
        :param abstract:                    [str] abstract of document
        :param categories:                  [list of str] words/phrases describing the categories the document should be
                                                classified into
        :return (category, similarities):   ([str] category the document is classified into,
                                             [array(float)] array of similarities to each category)
        """
        abstract = abstract or ""

        text = title + abstract + title
        tokenized_doc = tokenize_text(text)
        if not tokenized_doc:
            return None, None

        # lowercase and remove stopwords and words that are not in the model
        tokens = [t.lower() for t in tokenized_doc
                  if t.lower() not in NLTK_CORPUS_STOPWORDS and t.lower() in self.w2v_model.wv.vocab]

        doc_vec = numpy.array(self.w2v_model.wv[tokens]).mean(axis=0)
        doc_vec = doc_vec/numpy.linalg.norm(doc_vec)
        cat_vecs = numpy.array([self.get_ngram_representation(cat.lower()) for cat in categories])
        cat_vecs = cat_vecs/numpy.linalg.norm(cat_vecs, axis=1)[:, None]

        similarities = numpy.dot(cat_vecs, doc_vec)

        # VS: if multiclass classification is desired we could calculate probabilities from softmax and e.g. select
        # all categories with cumulative probabiliy > some threshold
        ind = int(numpy.argmax(similarities))

        return categories[ind], similarities
