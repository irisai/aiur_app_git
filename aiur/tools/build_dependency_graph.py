import os
import simplejson
from collections import defaultdict

from django.conf import settings

from aiur.nosql_client.articles_service import CoreBackupArticles
from aiur.nosql_client.paper_model import PaperAbstract


# TODO: Separate this class to two classes - DependencyGraph and DependencyGraphBuilder -
# as it is now performing both tasks
class DependencyGraphBuilder:
    def __init__(self, data_folder, filepath=None, verbose=True):
        self.data_folder = data_folder
        self.filepath = filepath
        self.verbose = verbose
        self.id_to_paper = {}
        self.articles_service = CoreBackupArticles()

    def _get_filepaths(self):
        files = os.listdir(self.data_folder)
        filepaths = defaultdict(dict)
        for file in files:
            kind = None
            if file.endswith(".pdf"):
                kind = "pdf"
            elif file.endswith(".pdf.json"):
                kind = "json"
            if kind:
                stem = file.replace(".json", "").replace(".pdf", "")
                filepaths[stem][kind] = file
        return filepaths

    def to_dict(self):
        output_dict = {"data_folder": self.data_folder,
                       "verbose": self.verbose,
                       "id_to_paper": {paper_id: p.to_dict() for paper_id, p in self.id_to_paper.items()}}
        return output_dict

    def save(self, filepath=None):
        filepath = filepath if filepath else self.filepath

        if not filepath:
            raise ValueError('Please provide `filepath` to which the graph will be saved')

        with open(filepath, "w", encoding="utf8") as out_file:
            simplejson.dump(self.to_dict(), out_file)

    @classmethod
    def load(cls, filepath):
        with open(filepath, "r", encoding="utf8") as in_file:
            graph_dict = simplejson.load(in_file)
        builder = cls(graph_dict["data_folder"], filepath=filepath, verbose=graph_dict.get("verbose", False))
        id_to_paper_dicts = graph_dict.get("id_to_paper", {})
        for paper_id, paper_dict in id_to_paper_dicts.items():
            builder.id_to_paper[paper_id] = PaperAbstract.from_dict(paper_dict)
        # explicitly turn references from ids into PaperAbstract objects
        for paper_id, paper in builder.id_to_paper.items():
            new_children = [builder.id_to_paper[ref] if isinstance(ref, str) else ref for ref in paper.children]
            paper.children = new_children
        return builder

    def get_paper_node_from_article_dict(self, article_dict):
        paper = self.id_to_paper.get(article_dict[CoreBackupArticles.IDENTIFIER])
        if not paper:
            paper = PaperAbstract(None, None)
            paper.update_from_article_dict(article_dict)
            self.id_to_paper[paper.identifier] = paper
        return paper

    def create_article_from_ES(self, title, year, validate_links=True):

        output_article = {CoreBackupArticles.TITLE: title,
                          CoreBackupArticles.YEAR: year,
                          CoreBackupArticles.IDENTIFIER: "_".join(title.lower().split()[:5]) + "_{}".format(year)}
        # fetch metadata
        response = self.articles_service.search({"title": title, "year": year})
        if not response['hits']['hits']:
            if self.verbose:
                print("no results for title and year, trying just title")
            response = self.articles_service.search({"title": title})
            if not response['hits']['hits']:
                if self.verbose:
                    print("article not found on ES")
                return output_article

        articles = [doc['_source']['doc'] for doc in response['hits']['hits']]

        # TODO order articles by identifier preference (doi before oai before rest)
        # TODO implement function that yields title in most general format to prevent miscomparisons
        #  due punctuation, white space, etc
        ref_title = title.lower()
        for article in articles:
            is_valid, prepared_article = self.articles_service.prepare_article(article, validate_links=validate_links)
            # if not is_valid:
            #     continue
            current_title = output_article[CoreBackupArticles.TITLE].lower()
            if ref_title == current_title:
                output_article = prepared_article
                break

        return output_article

    def build_nodes_from_files(self):
        # if not recreate and os.path.isfile(self.output_filepath):
        #     self.load()
        #     return

        filepaths = self._get_filepaths()

        # create initial nodes for each paper
        for file in filepaths.values():
            json_file = file.get("json")
            if not json_file:
                continue
            filepath = os.path.join(self.data_folder, json_file)

            with open(filepath, "r", encoding="utf8") as fh:
                parsed_article = simplejson.load(fh, encoding='utf8')

            if self.verbose:
                print(80 * "-")
                print(json_file)

            title = parsed_article["metadata"]["title"]
            year = parsed_article["metadata"]["year"]

            if not title:
                if self.verbose:
                    print("missing title, skipping")
                continue
            if not year:
                if self.verbose:
                    print("missing year, skipping")
                continue
            if self.verbose:
                print()
                print("title: {}".format(title))
                print("year: {}".format(year))

            prepared_article = self.create_article_from_ES(title, year, False)
            paper = self.get_paper_node_from_article_dict(prepared_article)
            paper.children = parsed_article['metadata']['references']
            # id_to_references[paper.identifier] = parsed_article['metadata']['references']

            if self.verbose:
                print()
                print(paper.to_dict())

    def add_vertex(self, paper_abstract):
        """Add vertex, but use only the children that
        are currently in the graph.
        """
        new_children = []
        for child in paper_abstract.children:
            if isinstance(child, PaperAbstract):
                child_id = child.identifier
            elif isinstance(child, dict):
                child_id = child.get('identifier')
            elif isinstance(child, str):
                child_id = child
            else:
                raise ValueError('Only `PaperAbstract`, `dict` and `str` child types are allowed')

            if child_id in self.id_to_paper:
                new_children.append(self.id_to_paper[child_id])

        paper_abstract.children = new_children
        self.id_to_paper[paper_abstract.identifier] = paper_abstract

    def build_graph(self, recreate_nodes=False):
        if recreate_nodes or not self.id_to_paper:
            self.build_nodes_from_files()

        if self.verbose:
            print(80 * "=")
            print()
            print("FILLING IN REFERENCES")
            print()
        # fill in connections
        for paper_id, source_paper in self.id_to_paper.items():

            references = source_paper.children

            if self.verbose:
                print(80 * "-")
                print("{}: {} potential refs".format(paper_id, len(references)))

            new_references = []
            for ref in references:
                if isinstance(ref, PaperAbstract):
                    new_references.append(ref)
                    continue

                title = ref.get("title")
                year = ref.get("year")

                if not title:
                    print("missing title, skipping")
                    continue
                if not year:
                    print("missing year, skipping")
                    continue

                prepared_article = self.create_article_from_ES(title, year, False)
                ref_paper = self.id_to_paper.get(prepared_article[CoreBackupArticles.IDENTIFIER])
                if ref_paper:
                    new_references.append(ref_paper)
            source_paper.children = new_references
            if self.verbose:
                print()
                print("{}/{} references added".format(len(source_paper.children), len(references)))

            print(source_paper.to_dict())


def main():
    if os.path.isfile(settings.GRAPH_FILEPATH):
        builder = DependencyGraphBuilder.load(settings.GRAPH_FILEPATH)
    else:
        builder = DependencyGraphBuilder(settings.DATA_FOLDER)
        builder.build_nodes_from_files()
        builder.save(settings.GRAPH_FILEPATH)

    builder.build_graph(recreate_nodes=False)
    builder.save(settings.GRAPH_FILEPATH)


if __name__ == '__main__':
    main()
