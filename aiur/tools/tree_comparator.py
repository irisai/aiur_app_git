class TreeComparator:
    def __init__(self, original, review):
        self.original = original
        self.review = review

    @staticmethod
    def mark_all_children_with_type(element, type):
        for child in element["children"]:
            child["metadata"]["type"] = type
            TreeComparator.mark_all_children_with_type(child, type)

    @staticmethod
    def update_tree_diff_metadata(original, review, original_metadata, review_metadata, reference):
        original_children_ids = {child["ord_id"]: child for child in original["children"]}
        for child in review["children"]:
            child["metadata"] = child.get("metadata", dict())
            child_id = child.get("ord_id")
            original_child = original_children_ids.get(child_id)
            if original_child:
                TreeComparator.update_tree_diff_metadata(original_child, child, original_metadata, review_metadata,
                                                         reference)
                original_child["checked"] = True
            elif original_metadata.get(child_id):
                # MOVED element
                child["metadata"]["type"] = 'new'
                child["metadata"]["currentParent"] = review["ord_id"]
                child["metadata"]["previousParent"] = original_metadata.get(child_id)["parent"]
                child["parent"] = review["ord_id"]
                child["ord_id"] = reference.new_ord_id
                print("Set id: " + str(reference.new_ord_id))
                review_metadata[reference.new_ord_id] = child
                reference.new_ord_id += 1
                TreeComparator.update_tree_diff_metadata(original_metadata.get(child_id), child, original_metadata,
                                                         review_metadata, reference)
            else:
                # NEW element added
                child["metadata"]["type"] = 'new'
        for _id, child in original_children_ids.items():
            if "checked" not in child or not child["checked"]:
                child["metadata"] = child.get("metadata", dict())
                if review_metadata.get(_id):
                    # MOVED element
                    child["metadata"]["type"] = 'moved'
                    child["metadata"]["currentParent"] = review_metadata.get(_id)["parent"]
                    child["metadata"]["previousParent"] = original["ord_id"]
                    review["children"].append(child)
                    TreeComparator.update_tree_diff_metadata(child, review_metadata.get(_id), original_metadata,
                                                             review_metadata, reference)
                else:
                    # DELETED element
                    child["metadata"]["type"] = 'deleted'
                    TreeComparator.mark_all_children_with_type(child, 'deleted')
                    review["children"].append(child)

    @staticmethod
    def append_children_metadata(element, identifier_to_child):
        for child in element["children"]:
            child["parent"] = element["ord_id"]
            if 'ord_id' in child:
                identifier_to_child[child["ord_id"]] = child
            TreeComparator.append_children_metadata(child, identifier_to_child)

    @staticmethod
    def build_tree_metadata(tree):
        identifier_to_child = dict()
        tree["parent"] = None
        identifier_to_child[tree["ord_id"]] = tree
        TreeComparator.append_children_metadata(tree, identifier_to_child)
        return identifier_to_child

    def compare_dependency_trees(self):
        if self.original["identifier"] != self.review["identifier"]:
            raise ValueError("Cannot compare trees with different roots!")

        original_metadata = self.build_tree_metadata(self.original)
        review_metadata = self.build_tree_metadata(self.review)

        self.new_ord_id = max(max(review_metadata.keys()), max(original_metadata.keys())) + 1
        self.update_tree_diff_metadata(self.original, self.review, original_metadata, review_metadata, self)

        return self.review

    @staticmethod
    def check_for_children_differences(original, review):
        original_children_ids = {child["identifier"]: child for child in original["children"]}
        review_children_ids = {child["identifier"]: child for child in review["children"]}
        diff = set(original_children_ids.keys()).symmetric_difference(set(review_children_ids.keys()))
        if len(diff):
            return True
        for child in review["children"]:
            child_id = child.get("identifier")
            original_child = original_children_ids.get(child_id)
            children_diff = TreeComparator.check_for_children_differences(original_child, child)
            if children_diff:
                return True
        return False

    def check_for_differences(self):
        if self.original["identifier"] != self.review["identifier"]:
            return True

        return self.check_for_children_differences(self.original, self.review)
