from argparse import ArgumentParser
from collections import Counter
from operator import itemgetter

import simplejson

from aiur.tools.document_classifier import DocumentAnalyzer


def update_tags_in_graph(graph, document_analyzer, categories, verbose=True):

    id_to_paper = graph["id_to_paper"]
    tags = []
    for paper in id_to_paper.values():

        paper["tag"] = None
        title = paper["title"]
        abstract = paper["abstract"]
        tag, sims = document_analyzer.classify_doc_into_categories(title, abstract, categories)
        paper["tag"] = tag

        if verbose:
            tags.append(tag)
            print(80 * "=")
            print("title: {}".format(title))
            print()
            print("abstract: {}".format(abstract))
            print()
            print(tag)
            print(list(zip(categories, sims)))

    if verbose:
        ctr = Counter(tags)
        total = sum(ctr.values())
        tag_ct_pairs = sorted(ctr.items(), key=itemgetter(1), reverse=True)

        print()
        for tag, ct in tag_ct_pairs:
            print("{}: {}/{} ({:2.2f}%)".format(tag, ct, total, 100 * ct / total))


def main():
    arg_parser = ArgumentParser("Command line interface for hierarchy building hyperparameter tuning")
    arg_parser.add_argument("--config-file-loc", required=True,
                            type=str,
                            help="absolute path to a JSON file specifying values of this script's config params",
                            metavar="CONFIG_FILE_LOC")

    command_line_args = arg_parser.parse_args()
    with open(command_line_args.config_file_loc, "r") as file:
        dict_config_params = simplejson.load(file)

    categories = dict_config_params["classification categories"]
    document_analyzer = DocumentAnalyzer(w2v_filename=dict_config_params["w2v model filepath"])
    with open(dict_config_params["input graph json filepath"], "r") as input_file:
        graph = simplejson.load(input_file)

    update_tags_in_graph(graph, document_analyzer, categories)

    with open(dict_config_params["output graph json filepath"], "w") as output_file:
        simplejson.dump(graph, output_file)


if __name__ == '__main__':
    main()
