from iris_auth.views import JWTObtainViewBase, JWTRefreshViewBase

from aiur.views.users import create_payload_for_user


class JWTObtainView(JWTObtainViewBase):
    def create_payload_for_user(self, user, request):
        return create_payload_for_user(user, request)


class JWTRefreshObtainView(JWTRefreshViewBase):
    def create_payload_for_user(self, user, request):
        return create_payload_for_user(user, request)
