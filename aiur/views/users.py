import logging

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.core.validators import EmailValidator
from django.template.loader import render_to_string
from django.utils import timezone
from rest_framework import viewsets, status
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from rest_framework_jwt.utils import jwt_payload_handler

from iris_auth.permissions import IsAuthenticatedClient, IsAuthenticatedUser, IsAuthenticatedUserStaff
from iris_auth.utils import (jwt_encode_handler, jwt_get_secret_key, get_jwt_duration_in_mins,
                             get_password, password_policy_check)

from aiur.models import Expertise
from aiur.serializers import UserSerializer

logger = logging.getLogger('app.logger')

# TODO: this should be generated and not hardcoded
BASE_64_IMAGE = '''data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAIAAAD/gAIDAAABS0lEQVR4nO3bw
                U3DQBBA0QRxoS9aoSpaoS5zpQILf9Zr1tJ79yiTr/VhtM5z27YHx7z89wB3IlYgViBWIFYgViBW8Dry4Y+vt7Pm+Hz/Xue7
                9jhZgViBWIFYgViBWIFYgViBWIFYgViBWMHQIn1kIz1xAT7iz0vyEU5WIFYgViBWIFYgViBWIFYgViBWsLvuHFxTpq4XM4z
                8LicrECsQKxArECsQKxArECsQKxArECsYugp7XH7T9aup8zxH/ui02rI9ex6PYSBWIFYgViBWIFYgViBWIFYgVjC6G55itb
                Vpz/R3Sq80ex6PYSBWIFYgViBWIFYgViBWIFYgVnDFVdhqXIVdQaxArECsQKxArECsQKxArECsQKxArGBokT7LXS5ZnaxAr
                ECsQKxArECsQKxArECsQKxArGCJ3fAunKxArECsQKxArECs4AeDGjUQd1FJVgAAAABJRU5ErkJggg=='''


class UserViewset(viewsets.ModelViewSet):
    """
    List all users, or create a new user.
    """
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticatedClient,)
    # filter_backends = (SearchFilter, OrderingFilter, RangeFilter, UserTypeFilterBackend)
    # ordering_fields = ('id', 'email', 'username', 'first_name', 'last_name', 'last_login', 'date_joined')
    # search_fields = ('id', 'email', 'username', 'first_name', 'last_name')
    # filter_fields = ('date_joined', 'last_login', 'id')

    def retrieve(self, request, pk):
        if pk != str(request.user.id):
            return Response(
                {'status': 'Cannot access properties of another user!'},
                status=status.HTTP_403_FORBIDDEN
            )

        return super(UserViewset, self).retrieve(request, pk)

    def get_permissions(self):
        if self.action in ['create']:
            return [IsAuthenticatedClient()]
        elif self.action in ['retrieve', 'update']:
            return [IsAuthenticatedUser()]
        return [IsAuthenticatedUserStaff()]

    def create(self, request, *args, **kwargs):
        # TODO: rewrite this using UserSerializer.is_valid() and UserSerializer.save() methods
        required_fields = ['email', 'first_name', 'position_in_company', 'expertises', 'seniority']
        for field in required_fields:
            if not request.data.get(field):
                return Response(
                    {'status': '{} argument is mandatory'.format(field)},
                    status=status.HTTP_400_BAD_REQUEST
                )

        expertises = Expertise.objects.filter(name__in=[exp.lower() for exp in request.data['expertises']])

        User = get_user_model()
        validator = EmailValidator()
        try:
            email = request.data['email'].lower()
            validator(email)
            if not User.objects.filter(email__iexact=email).exists():
                plain_pass = get_password()
                first_name_max_length = User._meta.get_field('first_name').max_length
                first_name = request.data['first_name'][:first_name_max_length]
                # TODO: add check in login if user is_active and forbid login for inactive users
                user = User.objects.create_user(
                    email=email,
                    password=plain_pass,
                    first_name=first_name,
                    position_in_company=request.data['position_in_company'],
                    seniority=request.data['seniority'],
                )

                for expertise in expertises:
                    user.expertises.add(expertise)

                token = create_token(user, request)
                context = {
                    'token': token,
                    'user_id': user.pk,
                    'client_url': settings.IRIS_CLIENT_URL,
                    'name': user.first_name,
                    'duration': get_jwt_duration_in_mins()
                }

                msg_plain = render_to_string('email/registration.txt', context=context)
                msg_html = render_to_string('email/registration.html', context=context)
                send_mail(
                    'Welcome to Iris.ai',
                    msg_plain,
                    'noreply@iris.ai',
                    [user.email],
                    html_message=msg_html,
                )

                serializer = self.serializer_class(user, context={'request': request})
                logger.debug('User with id {} created.'.format(user.pk))
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                logger.error('User already exists!')
                raise ParseError(detail='User already exists!')
        except ValidationError as e:
            logger.error(e)
            raise ParseError(detail=e)

    def update(self, request, pk=None):
        if pk != str(request.user.id):
            return Response(
                {'status': 'Cannot change properties of another user!'},
                status=status.HTTP_403_FORBIDDEN
            )

        user_data = dict(request.data)
        protected_fields = dict(
            email=request.user.email,
            is_staff=request.user.is_staff,
            is_superuser=request.user.is_superuser,
        )

        updated_expertises = []
        if 'expertises' in user_data:
            expertises = [exp.lower() for exp in user_data['expertises']]
            updated_expertises = Expertise.objects.filter(name__in=expertises)
            del user_data['expertises']

        user_data.update(protected_fields)

        User = get_user_model()
        if 'password' in request.data:
            pass_check = False
            if 'old_password' in request.data:
                pass_check = request.user.check_password(request.data['old_password'])
            # if system is True, password check is skipped, this may be security breach
            if 'system' in request.data and request.data['system'] is True:
                pass_check = True
            if not pass_check:
                return Response({'status': 'Password check failed!'}, status=status.HTTP_401_UNAUTHORIZED)
            logger.debug('Password check successful!')
            new_password = request.data['password']
            if not password_policy_check(new_password):
                return Response({'status': 'Password policy violation!'}, status=status.HTTP_400_BAD_REQUEST)
            request.user.set_password(new_password)
            # TODO: add last_password_change to User and implement password reset functionality
            # request.user.last_password_change = timezone.now()
            logger.debug('Set new password')

        serializer = UserSerializer(request.user, data=user_data, context={'request': request}, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        user = User.objects.get(id=request.user.id)

        if updated_expertises:
            user.expertises.clear()
            for expertise in updated_expertises:
                user.expertises.add(expertise)

        token = create_token(user, request)
        logger.debug('User {} data updated!'.format(request.user.id))
        return Response({'token': token})


def create_payload_for_user(user, request):
    payload = jwt_payload_handler(user)
    if request.data.get('remember_me') is True:
        payload['exp'] = timezone.now() + settings.JWT_AUTH['CLIENT_JWT_EXPIRATION_DELTA']
    additional_data = UserSerializer(user, context={'request': request}).data
    payload.update(additional_data)
    payload['type'] = 'user'
    payload['image'] = BASE_64_IMAGE

    return payload


def create_token(user, request):
    payload = create_payload_for_user(user, request)
    secret_key = jwt_get_secret_key(request)
    return jwt_encode_handler(payload, secret_key)
