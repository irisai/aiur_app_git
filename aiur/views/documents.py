import logging
import simplejson
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.decorators import list_route, detail_route
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.exceptions import ParseError, ValidationError
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework import status

from django.db.models import Q
from django.utils import timezone

from aiur.filter_backends import PerUserFilterBackend
from aiur.models import Document, Tree, Expertise
from aiur.serializers import DocumentSerializer, TreeSerializer
from aiur.tools.tree_comparator import TreeComparator
from aiur.tools.tree_utils import dfs_add_ord_ids, dfs_find_by_ord_id
from aiur.tools.reference_extractor import get_binary_pdf, build_document_with_references, add_dependency_tree_to_graph

from iris_auth.permissions import IsAuthenticatedUser

__author__ = 'Viktor Botev <victor@iris.ai>'
__copyright__ = 'Copyright (C) 2017 IRIS.AI'
__version__ = '0.1'

logger = logging.getLogger('app.logger')

MIN_APPROVALS_FOR_DOC_INTEGRATION = 3


class DocumentsViewset(viewsets.ModelViewSet):

    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    permission_classes = (IsAuthenticatedUser,)
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter, PerUserFilterBackend)
    ordering_fields = ('id', 'status', 'year', 'created_at', 'modified_at',)
    search_fields = ('id', 'title', 'authors', 'fulltext_link', 'short_name', 'document_identifier')
    filter_fields = ('status',)

    def create(self, request):
        if 'link' not in request.data:
            raise ParseError(detail='Post data should be in JSON format and containing a valid link object.')

        link = request.data['link']
        if not link.endswith('.pdf'):
            raise ParseError(detail='Link should lead to a PDF document.')

        try:
            dependency_tree = self.create_document_and_submission_tree(get_binary_pdf(link), request)
        except ValueError as e:
            print(e)
            return Response({'error': 'Document like this already exists!'}, status=status.HTTP_400_BAD_REQUEST)

        return Response(dependency_tree, status=status.HTTP_201_CREATED)

    @list_route(methods=['post'], url_path='file_upload', parser_classes=[MultiPartParser])
    def create_via_file(self, request):
        file_obj = request.FILES.get('file')

        try:
            dependency_tree = self.create_document_and_submission_tree(file_obj, request)
        except ValueError as e:
            print(e)
            return Response({'error': 'Document like this already exists!'}, status=status.HTTP_400_BAD_REQUEST)

        return Response(dependency_tree, status=status.HTTP_201_CREATED)

    @staticmethod
    def create_document_and_submission_tree(file_data, request):
        article = build_document_with_references(file_data)

        if len(article.children) == 0:
            raise ValueError("This document doesn't have references.")

        already_exist = Document.objects.filter(document_identifier=article.identifier).exists()
        if already_exist:
            raise ValueError('This document already exists')

        document = Document.create_document_from_paper_abstract(article, request.user)

        prepared_tree = article.to_dict(depth=3, depth_explicit=True)
        dfs_add_ord_ids(prepared_tree)
        dependency_tree = simplejson.dumps(prepared_tree)
        tree = Tree()
        tree.owner = request.user
        tree.dependency_tree = dependency_tree

        document.save()
        tag = Expertise.objects.get(name=article.tag)
        document.tags.add(tag)

        tree.document = document
        tree.save()

        prepared_tree['tree_id'] = tree.id
        prepared_tree['document_id'] = document.id

        return simplejson.dumps(prepared_tree)

    def list(self, request, *args, **kwargs):
        if 'reviewed' in request.query_params and request.query_params['reviewed']:
            my_reviews = Tree.objects.all().filter(owner=request.user, type=Tree.REVIEW).values('document')
            documents = self.queryset.filter(id__in=my_reviews)
            return self.filter_and_paginated_not_owned_documents(documents, request)
        elif 'for_review' in request.query_params and request.query_params['for_review']:
            other_documents = Document.objects.all().filter(~Q(owner=request.user) & ~Q(status=Document.INTEGRATED))
            return self.filter_and_paginated_not_owned_documents(other_documents, request)
        else:
            return super().list(request, *args, **kwargs)

    def filter_and_paginated_not_owned_documents(self, documents, request):
        self.filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
        result_queryset = self.filter_queryset(documents)
        self.filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter, PerUserFilterBackend)
        page = self.paginate_queryset(result_queryset)
        if page is not None:
            serializer = DocumentSerializer(page, context={'request': request}, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = DocumentSerializer(documents, context={'request': request}, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class TreesViewset(viewsets.ModelViewSet):

    queryset = Tree.objects.all()
    serializer_class = TreeSerializer
    permission_classes = (IsAuthenticatedUser,)
    filter_backends = (DjangoFilterBackend, OrderingFilter, PerUserFilterBackend)
    ordering_fields = ('id', 'created_at', 'modified_at',)
    filter_fields = ('status', 'type', 'document')

    def update(self, request, *args, **kwargs):
        if 'pk' not in kwargs or not kwargs['pk']:
            return Response({"status": "not found"}, status=status.HTTP_404_NOT_FOUND)

        current_tree = self.get_object()

        if not current_tree:
            return Response({"status": "not found"}, status=status.HTTP_404_NOT_FOUND)

        if current_tree.document.status == Document.INTEGRATED:
            return Response(
                {"status": "trees of integrated documents cannot be edited"}, status=status.HTTP_400_BAD_REQUEST
            )

        if current_tree.type == Tree.REVIEW:
            current_review = current_tree

            if request.data["status"] == Tree.SUBMITTED:
                # check for status approved
                original = self.queryset.filter(document=current_review.document, type=Tree.SUBMISSION).get()
                if not original:
                    raise ParseError(detail="Data is inconsistent!")

                original_dependency_tree = simplejson.loads(original.dependency_tree)
                current_review_dependency_tree = simplejson.loads(current_review.dependency_tree)
                comparator = TreeComparator(original_dependency_tree, current_review_dependency_tree)
                if not comparator.check_for_differences():
                    request.data["status"] = Tree.APPROVED
                    current_review.document.status = Document.UNDER_APPROVAL
                    current_review.document.save()
            serializer = TreeSerializer(current_review, data=request.data, context={'request': request})
            if serializer.is_valid():
                serializer.save()

                # now it may be necessary to transition the Document to integrated
                self.integrate_doc_if_needed(current_review.document)

                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            reviews = self.queryset.filter(document=current_tree.document, type=Tree.REVIEW)
            for review in reviews:
                review.status = Tree.IN_PROGRESS
                review.save()
            return super().update(request, *args, **kwargs)

    @detail_route(methods=['get'], url_path='compare')
    def compare_original_to_review(self, request, pk=None):
        current_review = self.get_object()

        if not current_review or current_review.type != Tree.REVIEW:
            raise ParseError(detail="You can compare only tree of type REVIEW!")

        original = self.queryset.filter(document=current_review.document, type=Tree.SUBMISSION).get()
        if not original:
            raise ParseError(detail="Data is inconsistent!")

        original_dependency_tree = simplejson.loads(original.dependency_tree)
        current_review_dependency_tree = simplejson.loads(current_review.dependency_tree)

        comparator = TreeComparator(original_dependency_tree, current_review_dependency_tree)
        diff_dependency_tree = comparator.compare_dependency_trees()

        return Response(diff_dependency_tree, status=status.HTTP_200_OK)

    @detail_route(methods=['post'], url_path='comment')
    def comment(self, request, pk=None):
        ord_id, comment_text = request.data.get('ord_id'), request.data.get('text')
        if not ord_id or not comment_text:
            raise ValidationError(detail='`ord_id` and `text` params should be provided')

        if not (0 < len(comment_text) <= 256):
            raise ValidationError(detail='comment length should be betwen 0 and 256')

        review_tree = self.get_object()
        submission_tree = self.queryset.get(document=review_tree.document, type=Tree.SUBMISSION)
        if review_tree.type != Tree.REVIEW:
            raise ValidationError(detail='You can add comments only to tree of type REVIEW!')

        if request.user not in [review_tree.owner, submission_tree.owner]:
            return Response(
                {'status': 'Adding a comment is allowed only to owners and reviewers!'},
                status=status.HTTP_403_FORBIDDEN
            )

        def add_comment(tree, ord_id, comment):
            dependency_tree = simplejson.loads(tree.dependency_tree)
            node = dfs_find_by_ord_id(dependency_tree, ord_id)
            if not node:
                return False

            if 'comments' not in node:
                node['comments'] = [comment]
            else:
                node['comments'].append(comment)
            tree.dependency_tree = simplejson.dumps(dependency_tree)
            tree.save()
            return True

        comment_to_add = {
            'text': comment_text,
            'owner_id': request.user.id,
            'timestamp': timezone.now().isoformat(),
        }

        # If adding a comment to deleted node, it will be added only to the submitted tree.
        # If adding a comment to untouched or moved, it will be added to the submitted and the review tree.
        submission_comment_added = add_comment(submission_tree, ord_id, comment_to_add)
        if not submission_comment_added:
            raise ValidationError(detail='Node with ord_id {} does not exist.'.format(ord_id))

        review_comment_added = add_comment(review_tree, ord_id, comment_to_add)

        logger.debug('Comment added to tree {}. Comment added to SUBMISSION tree: {} '
                     'Comment added to REVIEW tree {}'.format(pk, submission_comment_added, review_comment_added))

        return Response(comment_to_add, status=status.HTTP_201_CREATED)

    def integrate_doc_if_needed(self, document):
        """ Change status of document to Document.INTEGRATED and add it to the graph.
        """
        approved = self.queryset.filter(document=document, type=Tree.REVIEW, status=Tree.APPROVED)
        if approved.count() >= MIN_APPROVALS_FOR_DOC_INTEGRATION:
            document.status = Document.INTEGRATED
            document.save()

            # use the tree submitted by the owner of the document
            # as this will be the final version
            dependency_tree = document.trees.get(type=Tree.SUBMISSION).dependency_tree
            add_dependency_tree_to_graph(dependency_tree)
