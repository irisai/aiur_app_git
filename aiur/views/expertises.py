from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import viewsets

from aiur.models import Expertise
from aiur.serializers import ExpertiseSerializer


class ExpertiseViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Expertise.objects.all()
    serializer_class = ExpertiseSerializer
    permission_classes = [AllowAny]

    def list(self, request, *args, **kwargs):
        return Response(self.queryset.values_list('name', flat=True))
