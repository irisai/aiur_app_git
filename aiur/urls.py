from django.conf.urls import url, include
from rest_framework import routers
from aiur.views.login import JWTObtainView, JWTRefreshObtainView
from aiur.views.documents import DocumentsViewset, TreesViewset
from aiur.views.expertises import ExpertiseViewSet
from aiur.views.users import UserViewset

router = routers.DefaultRouter()
router.register(r'users', UserViewset)
router.register(r'documents', DocumentsViewset)
router.register(r'trees', TreesViewset)
router.register(r'expertises', ExpertiseViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api/token/$', JWTObtainView.as_view()),
    url(r'^api/token/refresh/$', JWTRefreshObtainView.as_view()),
]
