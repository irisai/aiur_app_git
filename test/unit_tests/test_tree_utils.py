import unittest

from aiur.tools.tree_utils import dfs_add_ord_ids, dfs_find_by_ord_id


class TestDFSFunctions(unittest.TestCase):
    def setUp(self):
        self.dependency_tree = {
            'title': 'Title', 'year': 2016, 'identifier': '1',
            'metadata': {'type': 'original'}, 'ord_id': 1,
            'children': [
                {
                    'title': 'Title', 'year': 2016, 'identifier': '2',
                    'metadata': {'type': 'original'}, 'ord_id': 2,
                    'children': [
                        {
                            'title': 'Title', 'year': 2016, 'identifier': '3',
                            'metadata': {'type': 'original'}, 'ord_id': 3,
                            'children': [

                            ]
                        },
                        {
                            'title': 'Title', 'year': 2016, 'identifier': '4',
                            'metadata': {'type': 'original'}, 'ord_id': 4,
                            'children': [
                                {
                                    'title': 'Title', 'year': 2016, 'identifier': '5',
                                    'metadata': {'type': 'original'}, 'ord_id': 5,
                                    'children': [

                                    ]
                                },

                            ]
                        },
                    ]
                },
                {
                    'title': 'Title', 'year': 2016, 'identifier': '6',
                    'metadata': {'type': 'original'}, 'ord_id': 6,
                    'children': [
                    ]
                },
                {
                    'title': 'Title', 'year': 2016, 'identifier': '7',
                    'metadata': {'type': 'original'}, 'ord_id': 7,
                    'children': [
                        {
                            'title': 'Title', 'year': 2016, 'identifier': '8',
                            'metadata': {'type': 'original'}, 'ord_id': 8,
                            'children': [
                                {
                                    'title': 'Title', 'year': 2016, 'identifier': '9',
                                    'metadata': {'type': 'original'}, 'ord_id': 9,
                                    'children': [
                                    ]
                                },
                            ]
                        },
                    ]
                },
            ]
        }

    def test_dfs_add_ord_ids(self):
        self.root = {
            'title': 'Title',
            'year': 2019,
            'identifier': '1',
            'metadata': {'type': 'original'},
            'children': []
        }
        empty_ord_id = dfs_add_ord_ids(self.root)
        self.assertEqual(empty_ord_id, 1)

        initial_ord_id = dfs_add_ord_ids(self.root, 5)
        self.assertEqual(initial_ord_id, 5)

        ord_id = dfs_add_ord_ids(self.dependency_tree)
        self.assertEqual(ord_id, 9)

    def test_dfs_find_by_ord_id(self):
        # children in dependency tree
        ord_id_1 = self.dependency_tree
        ord_id_2 = ord_id_1['children'][0]
        ord_id_3 = ord_id_2['children'][0]
        ord_id_4 = ord_id_2['children'][1]
        ord_id_5 = ord_id_4['children'][0]
        ord_id_6 = ord_id_1['children'][1]
        ord_id_7 = ord_id_1['children'][2]
        ord_id_8 = ord_id_7['children'][0]
        ord_id_9 = ord_id_8['children'][0]
        self.assertEqual(ord_id_1, dfs_find_by_ord_id(self.dependency_tree, 1))
        self.assertEqual(ord_id_2, dfs_find_by_ord_id(self.dependency_tree, 2))
        self.assertEqual(ord_id_3, dfs_find_by_ord_id(self.dependency_tree, 3))
        self.assertEqual(ord_id_4, dfs_find_by_ord_id(self.dependency_tree, 4))
        self.assertEqual(ord_id_5, dfs_find_by_ord_id(self.dependency_tree, 5))
        self.assertEqual(ord_id_6, dfs_find_by_ord_id(self.dependency_tree, 6))
        self.assertEqual(ord_id_7, dfs_find_by_ord_id(self.dependency_tree, 7))
        self.assertEqual(ord_id_8, dfs_find_by_ord_id(self.dependency_tree, 8))
        self.assertEqual(ord_id_9, dfs_find_by_ord_id(self.dependency_tree, 9))

        # not in dependency_tree
        self.assertIsNone(dfs_find_by_ord_id(self.dependency_tree, 10))
        self.assertIsNone(dfs_find_by_ord_id(self.dependency_tree, 11))
        self.assertIsNone(dfs_find_by_ord_id(self.dependency_tree, 15))
        self.assertIsNone(dfs_find_by_ord_id(self.dependency_tree, 'a'))


if __name__ == '__main__':
    unittest.main()
