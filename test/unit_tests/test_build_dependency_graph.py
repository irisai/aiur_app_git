import copy
import unittest

from aiur.nosql_client.paper_model import PaperAbstract
from aiur.tools.tree_comparator import TreeComparator
from aiur.tools.build_dependency_graph import DependencyGraphBuilder


class TestCompareDependencyTree(unittest.TestCase):

    def setUp(self):
        self.original_dependency_tree = {
            'title': 'Very Deep Multilingual Convolutional Neural Networks for LVCSR',
            'identifier': 'oai:arXiv.org:1509.08967',
            'name': 'very_deep_multilingual_convolutional_neural_oai:arXiv.org:1509.08967',
            'metadata': {'type': 'original'},
            'children': [{
                'title': 'Advances in Very Deep Convolutional Neural Networks for LVCSR',
                'identifier': 'oai:arXiv.org:1604.01792',
                'name': 'advances_in_very_deep_convolutional_oai:arXiv.org:1604.01792',
                'metadata': {'type': 'original'},
                'children': [{
                    'title': 'Very Deep Multilingual Convolutional Neural Networks for LVCSR',
                    'identifier': 'oai:arXiv.org:1509.08967',
                    'name': 'very_deep_multilingual_convolutional_neural_oai:arXiv.org:1509.08967',
                    'metadata': {'type': 'original'},
                    'children': [{
                        'title': 'Advances in Very Deep Convolutional Neural Networks for LVCSR',
                        'identifier': 'oai:arXiv.org:1604.01792',
                        'name': 'advances_in_very_deep_convolutional_oai:arXiv.org:1604.01792',
                        'metadata': {'type': 'original'},
                        'children': [],
                        'ord_id':4
                    }],
                    'ord_id':3
                }, {
                    'title': 'Deep Speech 2: End-to-End Speech Recognition in English and Mandarin',
                    'identifier': 'oai:arXiv.org:1512.02595',
                    'name': 'deep_speech_2:_end-to-end_speech_oai:arXiv.org:1512.02595',
                    'metadata': {'type': 'original'},
                    'children': [{
                        'title': 'Batch Normalized Recurrent Neural Networks',
                        'identifier': 'oai:arXiv.org:1510.01378',
                        'name': 'batch_normalized_recurrent_neural_networks_oai:arXiv.org:1510.01378',
                        'metadata': {'type': 'original'},
                        'children': [],
                        'ord_id':6
                    }],
                    'ord_id':5
                }],
                'ord_id':2
            }],
            'ord_id': 1
        }

    def test_moved_paper(self):
        self.review_dependency_tree = copy.deepcopy(self.original_dependency_tree)
        children_for_move = self.review_dependency_tree['children'][0]['children'].pop()
        moved_ord_id = children_for_move["ord_id"]
        self.review_dependency_tree['children'][0]['children'][0]['children'].append(children_for_move)
        comparator = TreeComparator(self.original_dependency_tree, self.review_dependency_tree)
        diff_dependency_tree = comparator.compare_dependency_trees()
        new_ord_id = children_for_move["ord_id"]
        diff_dependency_tree_metadata = TreeComparator.build_tree_metadata(diff_dependency_tree)

        for key, value in diff_dependency_tree_metadata.items():
            moved_element_metadata = value['metadata']
            metadata_type = 'moved' if key == moved_ord_id else ('new' if key == new_ord_id else 'original')
            self.assertEqual(moved_element_metadata['type'], metadata_type)

    def test_deleted_paper(self):
        self.review_dependency_tree = copy.deepcopy(self.original_dependency_tree)
        children_for_deletion = self.review_dependency_tree['children'][0]['children'][0]['children'].pop()
        comparator = TreeComparator(self.original_dependency_tree, self.review_dependency_tree)
        diff_dependency_tree = comparator.compare_dependency_trees()
        diff_dependency_tree_metadata = TreeComparator.build_tree_metadata(diff_dependency_tree)

        for key, value in diff_dependency_tree_metadata.items():
            moved_element_metadata = value['metadata']
            metadata_type = 'deleted' if key == children_for_deletion['ord_id'] else 'original'
            self.assertEqual(moved_element_metadata['type'], metadata_type)


class TestBuildDependencyGraph(unittest.TestCase):
    def setUp(self):
        self.builder = DependencyGraphBuilder('')
        self.child_id_1 = 'oai:arXiv.org:1609.03401'
        self.child_id_2 = 'oai:arXiv.org:1609.03402'
        self.child_id_3 = 'oai:arXiv.org:1609.03403'
        self.builder.id_to_paper = {
            self.child_id_1: PaperAbstract('Title1', 2000, identifier=self.child_id_1),
            self.child_id_2: PaperAbstract('Title2', 2000, identifier=self.child_id_2),
            self.child_id_3: PaperAbstract('Title3', 2000, identifier=self.child_id_3),
        }

    def test_add_vertex(self):
        vertex = PaperAbstract('New', 2015, identifier='oai:arXiv.org:1609.03404')
        vertex.children = [self.child_id_1, self.child_id_2, 'non_existent']

        self.builder.add_vertex(vertex)

        self.assertIn(vertex.identifier, self.builder.id_to_paper)

        inserted = self.builder.id_to_paper[vertex.identifier]
        self.assertEqual({child.identifier for child in inserted.children}, set([self.child_id_1, self.child_id_2]))


if __name__ == '__main__':
    unittest.main()
