import unittest

from aiur.tools.url_utils import check_url_existence, get_valid_url, order_for_full_text_link, fix_fulltext_url


class TestUrlUtils(unittest.TestCase):
    def test_check_url_existence(self):
        valid = check_url_existence('https://www.google.com/')
        invalid = check_url_existence('http://ivalid.url/?search=text')
        self.assertTrue(valid)
        self.assertFalse(invalid)

    def test_get_valid_url(self):
        urls = [
            'http://ivalid.url/?search=text',
            'https://www.google.com/',
            'https://core.ac.uk/display/132421253f3',
            'https://core.ac.uk/display/10889993'
        ]
        result = get_valid_url(urls)
        self.assertEqual(result, 'https://www.google.com/')

    def test_order_for_full_text_link(self):
        pdf_order = order_for_full_text_link('https://core.ac.uk/download/46603488.pdf')
        core_order = order_for_full_text_link('https://core.ac.uk/display/463488')
        search_query_order = order_for_full_text_link('https://core.ac.uk/search?q=1234')

        self.assertEqual(pdf_order, (True, False, False))
        self.assertEqual(core_order, (False, True, False))
        self.assertEqual(search_query_order, (False, False, True))

    def test_fix_fulltext_url(self):
        arxiv_link = 'https://arxiv.org/abs/2002.11101'
        other_link = 'https://core.ac.uk/display/10889993'

        arxiv_fix = fix_fulltext_url(arxiv_link)
        other_fix = fix_fulltext_url(other_link)

        self.assertEqual(arxiv_fix, 'https://arxiv.org/pdf/2002.11101.pdf')
        self.assertEqual(other_fix, other_link)
