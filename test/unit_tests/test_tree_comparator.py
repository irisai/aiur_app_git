import unittest


from aiur.tools.tree_comparator import TreeComparator


class TestTreeComparator(unittest.TestCase):
    def setUp(self):
        self.original = {
            'title': 'Very Deep Multilingual Convolutional Neural Networks for LVCSR',
            'year': 2016,
            'identifier': 'oai:arXiv.org:1509.08967',
            'journal': None,
            'fulltext_link': 'http://arxiv.org/pdf/1509.08967.pdf',
            'download_url': 'http://arxiv.org/pdf/1509.08967.pdf',
            'abstract': "Convolutional neural networks (CNNs) are a standard component of many current\nstate-of-the-art Large Vocabulary Continuous Speech Recognition (LVCSR)\nsystems. However, CNNs in LVCSR have not kept pace with recent advances in\nother domains where deeper neural networks provide superior performance. In\nthis paper we propose a number of architectural advances in CNNs for LVCSR.\nFirst, we introduce a very deep convolutional network architecture with up to\n14 weight layers. There are multiple convolutional layers before each pooling\nlayer, with small 3x3 kernels, inspired by the VGG Imagenet 2014 architecture.\nThen, we introduce multilingual CNNs with multiple untied layers. Finally, we\nintroduce multi-scale input features aimed at exploiting more context at\nnegligible computational cost. We evaluate the improvements first on a Babel\ntask for low resource speech recognition, obtaining an absolute 5.77% WER\nimprovement over the baseline PLP DNN by training our CNN on the combined data\nof six different languages. We then evaluate the very deep CNNs on the Hub5'00\nbenchmark (using the 262 hours of SWB-1 training data) achieving a word error\nrate of 11.8% after cross-entropy training, a 1.4% WER improvement (10.6%\nrelative) over the best published CNN result so far.Comment: Accepted for publication at ICASSP 201",  # noqa
            'fingerprint': [
                'CNNs', 'LVCSR', 'deep CNNs', 'network CNNs', 'multilingual CNNs', 'Recognition LVCSR',
                'LVCSR system', 'convolutional', 'speech recognition', 'convolutional layer',
                'Convolutional', 'Multilingual Convolutional', 'Multilingual', 'continuous speech',
                'recognition asr', 'deep convolutional', 'convolutional network',
                'convolutional neural', 'space-time coding', 'retinal', 'layer', 'convolutional code',
                'turbo code', 'turbo', 'bilingual', 'language learning', 'chinese language',
                'community', 'education', 'surface layer', 'language', 'algorithm', 'performance',
                'top layer', 'social', 'application', 'scheme', 'detection', 'feature', 'network'
            ],
            'name': 'very_deep_multilingual_convolutional_neural_oai:arXiv.org:1509.08967',
            'metadata': {'type': 'original'},
            'children': [{
                'title': 'Advances in Very Deep Convolutional Neural Networks for LVCSR',
                'year': 2016,
                'identifier': 'oai:arXiv.org:1604.01792', 'journal': None,
                'fulltext_link': 'http://arxiv.org/pdf/1604.01792.pdf',
                'download_url': 'http://arxiv.org/pdf/1604.01792.pdf',
                'abstract': 'Very deep CNNs with small 3x3 kernels have recently been shown to achievevery strong performance as acoustic models in hybrid NN-HMM speech recognitionsystems. In this paper we investigate how to efficiently scale these models to\nlarger datasets. Specifically, we address the design choice of pooling and\npadding along the time dimension which renders convolutional evaluation of\nsequences highly inefficient. We propose a new CNN design without timepadding\nand without timepooling, which is slightly suboptimal for accuracy, but has two\nsignificant advantages: it enables sequence training and deployment by allowing\nefficient convolutional evaluation of full utterances, and, it allows for batch\nnormalization to be straightforwardly adopted to CNNs on sequence data. Through\nbatch normalization, we recover the lost peformance from removing the\ntime-pooling, while keeping the benefit of efficient convolutional evaluation.\nWe demonstrate the performance of our models both on larger scale data than\nbefore, and after sequence training. Our very deep CNN model sequence trained\non the 2000h switchboard dataset obtains 9.4 word error rate on the Hub5\ntest-set, matching with a single model the performance of the 2015 IBM system\ncombination, which was the previous best published result.Comment: Proc. Interspeech 201',  # noqa
                'fingerprint': [
                    'CNNs', 'Convolutional', 'CNN', 'Convolutional Neural', 'Deep Convolutional', 'deep CNNs',
                    'deep CNN', 'Neural', 'Neural Networks', 'convolutional code', 'turbo code', 'turbo',
                    'space-time coding', 'retinal', 'community', 'skype', 'bbc', 'radio station',
                    'neural network', 'algorithm', 'performance', 'artificial neural', 'feedforward neural',
                    'language', 'education', 'application', 'scheme', 'detection'
                ],
                'name': 'advances_in_very_deep_convolutional_oai:arXiv.org:1604.01792',
                'metadata': {'type': 'original'},
                'children': [{
                    'title': 'Very Deep Multilingual Convolutional Neural Networks for LVCSR',
                    'year': 2016,
                    'identifier': 'oai:arXiv.org:1509.08967',
                    'journal': None,
                    'fulltext_link': 'http://arxiv.org/pdf/1509.08967.pdf',
                    'download_url': 'http://arxiv.org/pdf/1509.08967.pdf',
                    'abstract': "Convolutional neural networks (CNNs) are a standard component of many current\nstate-of-the-art Large Vocabulary Continuous Speech Recognition (LVCSR)\nsystems. However, CNNs in LVCSR have not kept pace with recent advances in\nother domains where deeper neural networks provide superior performance. In\nthis paper we propose a number of architectural advances in CNNs for LVCSR.\nFirst, we introduce a very deep convolutional network architecture with up to\n14 weight layers. There are multiple convolutional layers before each pooling\nlayer, with small 3x3 kernels, inspired by the VGG Imagenet 2014 architecture.\nThen, we introduce multilingual CNNs with multiple untied layers. Finally, we\nintroduce multi-scale input features aimed at exploiting more context at\nnegligible computational cost. We evaluate the improvements first on a Babel\ntask for low resource speech recognition, obtaining an absolute 5.77% WER\nimprovement over the baseline PLP DNN by training our CNN on the combined data\nof six different languages. We then evaluate the very deep CNNs on the Hub5'00\nbenchmark (using the 262 hours of SWB-1 training data) achieving a word error\nrate of 11.8% after cross-entropy training, a 1.4% WER improvement (10.6%\nrelative) over the best published CNN result so far.Comment: Accepted for publication at ICASSP 201",  # noqa
                    'fingerprint': [
                        'CNNs', 'LVCSR', 'deep CNNs', 'network CNNs', 'multilingual CNNs',
                        'Recognition LVCSR', 'LVCSR system', 'convolutional', 'speech recognition',
                        'convolutional layer', 'Convolutional', 'Multilingual Convolutional',
                        'Multilingual', 'continuous speech', 'recognition asr', 'deep convolutional',
                        'convolutional network', 'convolutional neural', 'space-time coding',
                        'retinal', 'layer', 'convolutional code', 'turbo code', 'turbo', 'bilingual',
                        'language learning', 'chinese language', 'community', 'education',
                        'surface layer', 'language', 'algorithm', 'performance', 'top layer',
                        'social', 'application', 'scheme', 'detection', 'feature', 'network'
                    ],
                    'name': 'very_deep_multilingual_convolutional_neural_oai:arXiv.org:1509.08967',
                    'metadata': {'type': 'original'},
                    'children': [{
                        'title': 'Advances in Very Deep Convolutional Neural Networks for LVCSR', 'year': 2016,
                        'identifier': 'oai:arXiv.org:1604.01792', 'journal': None,
                        'fulltext_link': 'http://arxiv.org/pdf/1604.01792.pdf',
                        'download_url': 'http://arxiv.org/pdf/1604.01792.pdf',
                        'abstract': 'Very deep CNNs with small 3x3 kernels have recently been shown to achieve\nvery strong performance as acoustic models in hybrid NN-HMM speech recognition\nsystems. In this paper we investigate how to efficiently scale these models to\nlarger datasets. Specifically, we address the design choice of pooling and\npadding along the time dimension which renders convolutional evaluation of\nsequences highly inefficient. We propose a new CNN design without timepadding\nand without timepooling, which is slightly suboptimal for accuracy, but has two\nsignificant advantages: it enables sequence training and deployment by allowing\nefficient convolutional evaluation of full utterances, and, it allows for batch\nnormalization to be straightforwardly adopted to CNNs on sequence data. Through\nbatch normalization, we recover the lost peformance from removing the\ntime-pooling, while keeping the benefit of efficient convolutional evaluation.\nWe demonstrate the performance of our models both on larger scale data than\nbefore, and after sequence training. Our very deep CNN model sequence trained\non the 2000h switchboard dataset obtains 9.4 word error rate on the Hub5\ntest-set, matching with a single model the performance of the 2015 IBM system\ncombination, which was the previous best published result.Comment: Proc. Interspeech 201',  # noqa
                        'fingerprint': [
                            'CNNs', 'Convolutional', 'CNN', 'Convolutional Neural', 'Deep Convolutional',
                            'deep CNNs', 'deep CNN', 'Neural', 'Neural Networks', 'convolutional code',
                            'turbo code', 'turbo', 'space-time coding', 'retinal', 'community', 'skype',
                            'bbc', 'radio station', 'neural network', 'algorithm', 'performance',
                            'artificial neural', 'feedforward neural', 'language', 'education', 'application',
                            'scheme', 'detection'
                        ],
                        'name': 'advances_in_very_deep_convolutional_oai:arXiv.org:1604.01792',
                        'metadata': {'type': 'original'},
                        'children': [],
                        'ord_id': 4
                    }],
                    'ord_id': 3
                }, {
                    'title': 'Deep Speech 2: End-to-End Speech Recognition in English and Mandarin',
                    'year': 2015, 'identifier': 'oai:arXiv.org:1512.02595', 'journal': None,
                    'fulltext_link': 'http://arxiv.org/pdf/1512.02595.pdf',
                    'download_url': 'http://arxiv.org/pdf/1512.02595.pdf',
                    'abstract': 'We show that an end-to-end deep learning approach can be used to recognize\neither English or Mandarin Chinese speech--two vastly different languages.\nBecause it replaces entire pipelines of hand-engineered components with neural\nnetworks, end-to-end learning allows us to handle a diverse variety of speech\nincluding noisy environments, accents and different languages. Key to our\napproach is our application of HPC techniques, resulting in a 7x speedup over\nour previous system. Because of this efficiency, experiments that previously\ntook weeks now run in days. This enables us to iterate more quickly to identify\nsuperior architectures and algorithms. As a result, in several cases, our\nsystem is competitive with the transcription of human workers when benchmarked\non standard datasets. Finally, using a technique called Batch Dispatch with\nGPUs in the data center, we show that our system can be inexpensively deployed\nin an online setting, delivering low latency when serving users at scale',  # noqa
                    'fingerprint': [
                        'Speech', 'End-to-End Speech', 'Mandarin', 'Speech Recognition',
                        'End-to-End', 'deep Speech', 'Mandarin Chinese', 'Recognition',
                        'end-to-end deep', 'speech sound', 'deep', 'voice', 'speaker',
                        'mandarin chinese', 'arabic', 'english learner', 'end-to-end delay',
                        'on-demand', 'qos', 'algorithm', 'performance', 'face recognition',
                        'speech recognition', 'pattern recognition', 'shallow', 'deep sea',
                        'deep layer', 'community', 'application', 'scheme', 'language', 'detection'
                    ],
                    'name': 'deep_speech_2:_end-to-end_speech_oai:arXiv.org:1512.02595',
                    'metadata': {'type': 'original'},
                    'children': [{
                        'title': 'Batch Normalized Recurrent Neural Networks',
                        'year': 2015,
                        'identifier': 'oai:arXiv.org:1510.01378', 'journal': None,
                        'fulltext_link': 'http://arxiv.org/pdf/1510.01378.pdf',
                        'download_url': 'http://arxiv.org/pdf/1510.01378.pdf',
                        'abstract': "Recurrent Neural Networks (RNNs) are powerful models for sequential data that\nhave the potential to learn long-term dependencies. However, they are\ncomputationally expensive to train and difficult to parallelize. Recent work\nhas shown that normalizing intermediate representations of neural networks can\nsignificantly improve convergence rates in feedforward neural networks . In\nparticular, batch normalization, which uses mini-batch statistics to\nstandardize features, was shown to significantly reduce training time. In this\npaper, we show that applying batch normalization to the hidden-to-hidden\ntransitions of our RNNs doesn't help the training procedure. We also show that\nwhen applied to the input-to-hidden transitions, batch normalization can lead\nto a faster convergence of the training criterion but doesn't seem to improve\nthe generalization performance on both our language modelling and speech\nrecognition tasks. All in all, applying batch normalization to RNNs turns out\nto be more challenging than applying it to feedforward networks, but certain\nvariants of it can still be beneficial",  # noqa
                        'fingerprint': [
                            'RNNs', 'batch', 'batch normalization', 'normalization', 'Networks RNNs',
                            'batch Normalized', 'transition batch', 'Neural', 'Recurrent Neural',
                            'Neural Networks', 'Recurrent', 'Normalized', 'batch reactor',
                            'batch culture', 'lab-scale', 'normalisation', 'repraesentatio',
                            'post-smoothed', 'network-based recommendation', 'correction',
                            'summation', 'neural network', 'artificial neural', 'normalised',
                            'feedforward neural', 'corrected', 'calculated', 'paroxysmal',
                            'hiv-associated', 'biopsy-proven', 'state', 'social', 'growth', 'school',
                            'learning', 'optimal', 'day', 'network', 'solution', 'feature', 'community'
                        ],
                        'name': 'batch_normalized_recurrent_neural_networks_oai:arXiv.org:1510.01378',
                        'metadata': {'type': 'original'},
                        'children': [],
                        'ord_id': 6
                    }],
                    'ord_id': 5
                }, {
                    'title': 'Rethinking the Inception Architecture for Computer Vision',
                    'year': 2015,
                    'identifier': 'oai:arXiv.org:1512.00567', 'journal': None,
                    'fulltext_link': 'http://arxiv.org/pdf/1512.00567.pdf',
                    'download_url': 'http://arxiv.org/pdf/1512.00567.pdf',
                    'abstract': 'Convolutional networks are at the core of most state-of-the-art computer\nvision solutions for a wide variety of tasks. Since 2014 very deep\nconvolutional networks started to become mainstream, yielding substantial gains\nin various benchmarks. Although increased model size and computational cost\ntend to translate to immediate quality gains for most tasks (as long as enough\nlabeled data is provided for training), computational efficiency and low\nparameter count are still enabling factors for various use cases such as mobile\nvision and big-data scenarios. Here we explore ways to scale up networks in\nways that aim at utilizing the added computation as efficiently as possible by\nsuitably factorized convolutions and aggressive regularization. We benchmark\nour methods on the ILSVRC 2012 classification challenge validation set\ndemonstrate substantial gains over the state of the art: 21.2% top-1 and 5.6%\ntop-5 error for single frame evaluation using a network with a computational\ncost of 5 billion multiply-adds per inference and with using less than 25\nmillion parameters. With an ensemble of 4 models and multi-crop evaluation, we\nreport 3.5% top-5 error on the validation set (3.6% error on the test set) and\n17.3% top-1 error on the validation set',  # noqa
                    'fingerprint': [
                        'Inception', 'top-5 error', 'convolutional', 'Inception Architecture',
                        'convolutional network', 'error', 'Vision', 'Computer Vision', 'network',
                        'Computer', 'Architecture', 'deep convolutional', 'convolutional code',
                        'turbo code', 'turbo', 'beginning', 'late 1980s', 'measurement error',
                        'systematic error', 'algorithm', 'performance', 'computer software',
                        'software', 'computer graphic', 'vision system', 'network structure',
                        'reality', 'communication network', 'idea', 'impact', 'system architecture',
                        'architecture', 'software architecture', 'cost', 'human', 'application',
                        'scheme', 'detection', 'strategy'
                    ],
                    'name': 'rethinking_the_inception_architecture_for_oai:arXiv.org:1512.00567',
                    'metadata': {'type': 'original'},
                    'children': [{
                        'title': 'Fast Algorithms for Convolutional Neural Networks',
                        'year': 2015,
                        'identifier': 'oai:arXiv.org:1509.09308',
                        'journal': None,
                        'fulltext_link': 'http://arxiv.org/pdf/1509.09308.pdf',
                        'download_url': 'http://arxiv.org/pdf/1509.09308.pdf',
                        'abstract': "Deep convolutional neural networks take GPU days of compute time to train on\nlarge data sets. Pedestrian detection for self driving cars requires very low\nlatency. Image recognition for mobile phones is constrained by limited\nprocessing resources. The success of convolutional neural networks in these\nsituations is limited by how fast we can compute them. Conventional FFT based\nconvolution is fast for large filters, but state of the art convolutional\nneural networks use small, 3x3 filters. We introduce a new class of fast\nalgorithms for convolutional neural networks using Winograd's minimal filtering\nalgorithms. The algorithms compute minimal complexity convolution over small\ntiles, which makes them fast with small filters and small batch sizes. We\nbenchmark a GPU implementation of our algorithm with the VGG network and show\nstate of the art throughput at batch sizes from 1 to 64",  # noqa
                        'fingerprint': [
                            'Convolutional', 'neural network', 'Convolutional Neural', 'network',
                            'Neural', 'Neural Networks', 'compute', 'compute minimal',
                            'convolutional code', 'turbo code', 'turbo', 'network structure',
                            'communication network', 'calculate', 'derive', 'artificial neural',
                            'feedforward neural', 'obtain', 'algorithm', 'performance', 'image',
                            'social', 'state', 'distance', 'application', 'scheme', 'detection', 'joint'
                        ],
                        'name': 'fast_algorithms_for_convolutional_neural_oai:arXiv.org:1509.09308',
                        'metadata': {'type': 'original'},
                        'children': [],
                        'ord_id': 8
                    }],
                    'ord_id': 7
                }, {
                    'title': 'Batch Normalized Recurrent Neural Networks',
                    'year': 2015,
                    'identifier': 'oai:arXiv.org:1510.01378',
                    'journal': None,
                    'fulltext_link': 'http://arxiv.org/pdf/1510.01378.pdf',
                    'download_url': 'http://arxiv.org/pdf/1510.01378.pdf',
                    'abstract': "Recurrent Neural Networks (RNNs) are powerful models for sequential data that\nhave the potential to learn long-term dependencies. However, they are\ncomputationally expensive to train and difficult to parallelize. Recent work\nhas shown that normalizing intermediate representations of neural networks can\nsignificantly improve convergence rates in feedforward neural networks . In\nparticular, batch normalization, which uses mini-batch statistics to\nstandardize features, was shown to significantly reduce training time. In this\npaper, we show that applying batch normalization to the hidden-to-hidden\ntransitions of our RNNs doesn't help the training procedure. We also show that\nwhen applied to the input-to-hidden transitions, batch normalization can lead\nto a faster convergence of the training criterion but doesn't seem to improve\nthe generalization performance on both our language modelling and speech\nrecognition tasks. All in all, applying batch normalization to RNNs turns out\nto be more challenging than applying it to feedforward networks, but certain\nvariants of it can still be beneficial",  # noqa
                    'fingerprint': [
                        'RNNs', 'batch', 'batch normalization', 'normalization', 'Networks RNNs',
                        'batch Normalized', 'transition batch', 'Neural', 'Recurrent Neural',
                        'Neural Networks', 'Recurrent', 'Normalized', 'batch reactor',
                        'batch culture', 'lab-scale', 'normalisation', 'repraesentatio',
                        'post-smoothed', 'network-based recommendation', 'correction', 'summation',
                        'neural network', 'artificial neural', 'normalised', 'feedforward neural',
                        'corrected', 'calculated', 'paroxysmal', 'hiv-associated', 'biopsy-proven',
                        'state', 'social', 'growth', 'school', 'learning', 'optimal', 'day',
                        'network', 'solution', 'feature', 'community'
                    ],
                    'name': 'batch_normalized_recurrent_neural_networks_oai:arXiv.org:1510.01378',
                    'metadata': {'type': 'original'},
                    'children': [{
                        'title': 'ImageNet Large Scale Visual Recognition Challenge',
                        'year': 2015,
                        'identifier': 'oai:arXiv.org:1409.0575',
                        'journal': None,
                        'fulltext_link': 'http://arxiv.org/pdf/1409.0575.pdf',
                        'download_url': None,
                        'abstract': 'The ImageNet Large Scale Visual Recognition Challenge is a benchmark in\nobject category classification and detection on hundreds of object categories\nand millions of images. The challenge has been run annually from 2010 to\npresent, attracting participation from more than fifty institutions.\n  This paper describes the creation of this benchmark dataset and the advances\nin object recognition that have been possible as a result. We discuss the\nchallenges of collecting large-scale ground truth annotation, highlight key\nbreakthroughs in categorical object recognition, provide a detailed analysis of\nthe current state of the field of large-scale image classification and object\ndetection, and compare the state-of-the-art computer vision accuracy with human\naccuracy. We conclude with lessons learned in the five years of the challenge,\nand propose future directions and improvements.Comment: 43 pages, 16 figures. v3 includes additional comparisons with PASCAL\n  VOC (per-category comparisons in Table 3, distribution of localization\n  difficulty in Fig 16), a list of queries used for obtaining object detection\n  images (Appendix C), and some additional reference',  # noqa
                        'fingerprint': [
                            'ImageNet', 'ImageNet Large', 'Challenge', 'Recognition Challenge',
                            'Recognition', 'Visual Recognition', 'Visual', 'Large', 'Scale Visual',
                            'object detection', 'Large Scale', 'Scale', 'pascal voc',
                            'image classification', 'texture-based', 'detection', 'challenge',
                            'small', 'dilemma', 'formidable challenge', 'face recognition',
                            'auditory', 'speech recognition', 'stage', 'pattern recognition',
                            'perceptual', 'visual stimulus', 'scale', 'huge', 'larger',
                            'scale model', 'rating scale', 'direct detection', 'detection method',
                            'detection technique', 'human', 'object', 'survival', 'algorithm',
                            'performance', 'category', 'grade'
                        ],
                        'name': 'imagenet_large_scale_visual_recognition_oai:arXiv.org:1409.0575',
                        'metadata': {'type': 'original'},
                        'children': [],
                        'ord_id': 10
                    }],
                    'ord_id': 9
                }],
                'ord_id': 2
            }],
            'ord_id': 1
        }
        self.review = {
            'title': 'Very Deep Multilingual Convolutional Neural Networks for LVCSR',
            'year': 2016,
            'identifier': 'oai:arXiv.org:1509.08967',
            'journal': None,
            'fulltext_link': 'http://arxiv.org/pdf/1509.08967.pdf',
            'download_url': 'http://arxiv.org/pdf/1509.08967.pdf',
            'abstract': "Convolutional neural networks (CNNs) are a standard component of many current\nstate-of-the-art Large Vocabulary Continuous Speech Recognition (LVCSR)\nsystems. However, CNNs in LVCSR have not kept pace with recent advances in\nother domains where deeper neural networks provide superior performance. In\nthis paper we propose a number of architectural advances in CNNs for LVCSR.\nFirst, we introduce a very deep convolutional network architecture with up to\n14 weight layers. There are multiple convolutional layers before each pooling\nlayer, with small 3x3 kernels, inspired by the VGG Imagenet 2014 architecture.\nThen, we introduce multilingual CNNs with multiple untied layers. Finally, we\nintroduce multi-scale input features aimed at exploiting more context at\nnegligible computational cost. We evaluate the improvements first on a Babel\ntask for low resource speech recognition, obtaining an absolute 5.77% WER\nimprovement over the baseline PLP DNN by training our CNN on the combined data\nof six different languages. We then evaluate the very deep CNNs on the Hub5'00\nbenchmark (using the 262 hours of SWB-1 training data) achieving a word error\nrate of 11.8% after cross-entropy training, a 1.4% WER improvement (10.6%\nrelative) over the best published CNN result so far.Comment: Accepted for publication at ICASSP 201",  # noqa
            'fingerprint': [
                'CNNs', 'LVCSR', 'deep CNNs', 'network CNNs', 'multilingual CNNs', 'Recognition LVCSR',
                'LVCSR system', 'convolutional', 'speech recognition', 'convolutional layer',
                'Convolutional', 'Multilingual Convolutional', 'Multilingual', 'continuous speech',
                'recognition asr', 'deep convolutional', 'convolutional network',
                'convolutional neural', 'space-time coding', 'retinal', 'layer', 'convolutional code',
                'turbo code', 'turbo', 'bilingual', 'language learning', 'chinese language',
                'community', 'education', 'surface layer', 'language', 'algorithm', 'performance',
                'top layer', 'social', 'application', 'scheme', 'detection', 'feature', 'network'
            ],
            'name': 'very_deep_multilingual_convolutional_neural_oai:arXiv.org:1509.08967',
            'metadata': {'type': 'original'},
            'children': [{
                'title': 'Advances in Very Deep Convolutional Neural Networks for LVCSR',
                'year': 2016,
                'identifier': 'oai:arXiv.org:1604.01792',
                'journal': None,
                'fulltext_link': 'http://arxiv.org/pdf/1604.01792.pdf',
                'download_url': 'http://arxiv.org/pdf/1604.01792.pdf',
                'abstract': 'Very deep CNNs with small 3x3 kernels have recently been shown to achievevery strong performance as acoustic models in hybrid NN-HMM speech recognitionsystems. In this paper we investigate how to efficiently scale these models to\nlarger datasets. Specifically, we address the design choice of pooling and\npadding along the time dimension which renders convolutional evaluation of\nsequences highly inefficient. We propose a new CNN design without timepadding\nand without timepooling, which is slightly suboptimal for accuracy, but has two\nsignificant advantages: it enables sequence training and deployment by allowing\nefficient convolutional evaluation of full utterances, and, it allows for batch\nnormalization to be straightforwardly adopted to CNNs on sequence data. Through\nbatch normalization, we recover the lost peformance from removing the\ntime-pooling, while keeping the benefit of efficient convolutional evaluation.\nWe demonstrate the performance of our models both on larger scale data than\nbefore, and after sequence training. Our very deep CNN model sequence trained\non the 2000h switchboard dataset obtains 9.4 word error rate on the Hub5\ntest-set, matching with a single model the performance of the 2015 IBM system\ncombination, which was the previous best published result.Comment: Proc. Interspeech 201',  # noqa
                'fingerprint': [
                    'CNNs', 'Convolutional', 'CNN', 'Convolutional Neural', 'Deep Convolutional', 'deep CNNs',
                    'deep CNN', 'Neural', 'Neural Networks', 'convolutional code', 'turbo code', 'turbo',
                    'space-time coding', 'retinal', 'community', 'skype', 'bbc', 'radio station',
                    'neural network', 'algorithm', 'performance', 'artificial neural', 'feedforward neural',
                    'language', 'education', 'application', 'scheme', 'detection'
                ],
                'name': 'advances_in_very_deep_convolutional_oai:arXiv.org:1604.01792', 'metadata': {'type': 'original'},
                'children': [{
                    'title': 'Very Deep Multilingual Convolutional Neural Networks for LVCSR',
                    'year': 2016,
                    'identifier': 'oai:arXiv.org:1509.08967',
                    'journal': None,
                    'fulltext_link': 'http://arxiv.org/pdf/1509.08967.pdf',
                    'download_url': 'http://arxiv.org/pdf/1509.08967.pdf',
                    'abstract': "Convolutional neural networks (CNNs) are a standard component of many current\nstate-of-the-art Large Vocabulary Continuous Speech Recognition (LVCSR)\nsystems. However, CNNs in LVCSR have not kept pace with recent advances in\nother domains where deeper neural networks provide superior performance. In\nthis paper we propose a number of architectural advances in CNNs for LVCSR.\nFirst, we introduce a very deep convolutional network architecture with up to\n14 weight layers. There are multiple convolutional layers before each pooling\nlayer, with small 3x3 kernels, inspired by the VGG Imagenet 2014 architecture.\nThen, we introduce multilingual CNNs with multiple untied layers. Finally, we\nintroduce multi-scale input features aimed at exploiting more context at\nnegligible computational cost. We evaluate the improvements first on a Babel\ntask for low resource speech recognition, obtaining an absolute 5.77% WER\nimprovement over the baseline PLP DNN by training our CNN on the combined data\nof six different languages. We then evaluate the very deep CNNs on the Hub5'00\nbenchmark (using the 262 hours of SWB-1 training data) achieving a word error\nrate of 11.8% after cross-entropy training, a 1.4% WER improvement (10.6%\nrelative) over the best published CNN result so far.Comment: Accepted for publication at ICASSP 201",  # noqa
                    'fingerprint': [
                        'CNNs', 'LVCSR', 'deep CNNs', 'network CNNs', 'multilingual CNNs',
                        'Recognition LVCSR', 'LVCSR system', 'convolutional', 'speech recognition',
                        'convolutional layer', 'Convolutional', 'Multilingual Convolutional',
                        'Multilingual', 'continuous speech', 'recognition asr', 'deep convolutional',
                        'convolutional network', 'convolutional neural', 'space-time coding',
                        'retinal', 'layer', 'convolutional code', 'turbo code', 'turbo', 'bilingual',
                        'language learning', 'chinese language', 'community', 'education',
                        'surface layer', 'language', 'algorithm', 'performance', 'top layer',
                        'social', 'application', 'scheme', 'detection', 'feature', 'network'
                    ],
                    'name': 'very_deep_multilingual_convolutional_neural_oai:arXiv.org:1509.08967',
                    'metadata': {'type': 'original'},
                    'children': [],
                    'ord_id': 3
                }, {
                    'title': 'Deep Speech 2: End-to-End Speech Recognition in English and Mandarin',
                    'year': 2015, 'identifier': 'oai:arXiv.org:1512.02595',
                    'journal': None,
                    'fulltext_link': 'http://arxiv.org/pdf/1512.02595.pdf',
                    'download_url': 'http://arxiv.org/pdf/1512.02595.pdf',
                    'abstract': 'We show that an end-to-end deep learning approach can be used to recognize\neither English or Mandarin Chinese speech--two vastly different languages.\nBecause it replaces entire pipelines of hand-engineered components with neural\nnetworks, end-to-end learning allows us to handle a diverse variety of speech\nincluding noisy environments, accents and different languages. Key to our\napproach is our application of HPC techniques, resulting in a 7x speedup over\nour previous system. Because of this efficiency, experiments that previously\ntook weeks now run in days. This enables us to iterate more quickly to identify\nsuperior architectures and algorithms. As a result, in several cases, our\nsystem is competitive with the transcription of human workers when benchmarked\non standard datasets. Finally, using a technique called Batch Dispatch with\nGPUs in the data center, we show that our system can be inexpensively deployed\nin an online setting, delivering low latency when serving users at scale',  # noqa
                    'fingerprint': [
                        'Speech', 'End-to-End Speech', 'Mandarin', 'Speech Recognition',
                        'End-to-End', 'deep Speech', 'Mandarin Chinese', 'Recognition',
                        'end-to-end deep', 'speech sound', 'deep', 'voice', 'speaker',
                        'mandarin chinese', 'arabic', 'english learner', 'end-to-end delay',
                        'on-demand', 'qos', 'algorithm', 'performance', 'face recognition',
                        'speech recognition', 'pattern recognition', 'shallow', 'deep sea',
                        'deep layer', 'community', 'application', 'scheme', 'language', 'detection'
                    ],
                    'name': 'deep_speech_2:_end-to-end_speech_oai:arXiv.org:1512.02595',
                    'metadata': {'type': 'original'},
                    'children': [{
                        'title': 'Batch Normalized Recurrent Neural Networks',
                        'year': 2015,
                        'identifier': 'oai:arXiv.org:1510.01378', 'journal': None,
                        'fulltext_link': 'http://arxiv.org/pdf/1510.01378.pdf',
                        'download_url': 'http://arxiv.org/pdf/1510.01378.pdf',
                        'abstract': "Recurrent Neural Networks (RNNs) are powerful models for sequential data that\nhave the potential to learn long-term dependencies. However, they are\ncomputationally expensive to train and difficult to parallelize. Recent work\nhas shown that normalizing intermediate representations of neural networks can\nsignificantly improve convergence rates in feedforward neural networks . In\nparticular, batch normalization, which uses mini-batch statistics to\nstandardize features, was shown to significantly reduce training time. In this\npaper, we show that applying batch normalization to the hidden-to-hidden\ntransitions of our RNNs doesn't help the training procedure. We also show that\nwhen applied to the input-to-hidden transitions, batch normalization can lead\nto a faster convergence of the training criterion but doesn't seem to improve\nthe generalization performance on both our language modelling and speech\nrecognition tasks. All in all, applying batch normalization to RNNs turns out\nto be more challenging than applying it to feedforward networks, but certain\nvariants of it can still be beneficial",  # noqa
                        'fingerprint': [
                            'RNNs', 'batch', 'batch normalization', 'normalization', 'Networks RNNs',
                            'batch Normalized', 'transition batch', 'Neural', 'Recurrent Neural',
                            'Neural Networks', 'Recurrent', 'Normalized', 'batch reactor',
                            'batch culture', 'lab-scale', 'normalisation', 'repraesentatio',
                            'post-smoothed', 'network-based recommendation', 'correction',
                            'summation', 'neural network', 'artificial neural', 'normalised',
                            'feedforward neural', 'corrected', 'calculated', 'paroxysmal',
                            'hiv-associated', 'biopsy-proven', 'state', 'social', 'growth', 'school',
                            'learning', 'optimal', 'day', 'network', 'solution', 'feature', 'community'
                        ],
                        'name': 'batch_normalized_recurrent_neural_networks_oai:arXiv.org:1510.01378',
                        'metadata': {'type': 'original'},
                        'children': [],
                        'ord_id': 6
                    }],
                    'ord_id': 5
                }, {
                    'title': 'Rethinking the Inception Architecture for Computer Vision',
                    'year': 2015,
                    'identifier': 'oai:arXiv.org:1512.00567',
                    'journal': None,
                    'fulltext_link': 'http://arxiv.org/pdf/1512.00567.pdf',
                    'download_url': 'http://arxiv.org/pdf/1512.00567.pdf',
                    'abstract': 'Convolutional networks are at the core of most state-of-the-art computer\nvision solutions for a wide variety of tasks. Since 2014 very deep\nconvolutional networks started to become mainstream, yielding substantial gains\nin various benchmarks. Although increased model size and computational cost\ntend to translate to immediate quality gains for most tasks (as long as enough\nlabeled data is provided for training), computational efficiency and low\nparameter count are still enabling factors for various use cases such as mobile\nvision and big-data scenarios. Here we explore ways to scale up networks in\nways that aim at utilizing the added computation as efficiently as possible by\nsuitably factorized convolutions and aggressive regularization. We benchmark\nour methods on the ILSVRC 2012 classification challenge validation set\ndemonstrate substantial gains over the state of the art: 21.2% top-1 and 5.6%\ntop-5 error for single frame evaluation using a network with a computational\ncost of 5 billion multiply-adds per inference and with using less than 25\nmillion parameters. With an ensemble of 4 models and multi-crop evaluation, we\nreport 3.5% top-5 error on the validation set (3.6% error on the test set) and\n17.3% top-1 error on the validation set',  # noqa
                    'fingerprint': [
                        'Inception', 'top-5 error', 'convolutional', 'Inception Architecture',
                        'convolutional network', 'error', 'Vision', 'Computer Vision', 'network',
                        'Computer', 'Architecture', 'deep convolutional', 'convolutional code',
                        'turbo code', 'turbo', 'beginning', 'late 1980s', 'measurement error',
                        'systematic error', 'algorithm', 'performance', 'computer software',
                        'software', 'computer graphic', 'vision system', 'network structure',
                        'reality', 'communication network', 'idea', 'impact', 'system architecture',
                        'architecture', 'software architecture', 'cost', 'human', 'application',
                        'scheme', 'detection', 'strategy'
                    ],
                    'name': 'rethinking_the_inception_architecture_for_oai:arXiv.org:1512.00567',
                    'metadata': {'type': 'original'},
                    'children': [{
                        'title': 'Fast Algorithms for Convolutional Neural Networks',
                        'year': 2015,
                        'identifier': 'oai:arXiv.org:1509.09308',
                        'journal': None,
                        'fulltext_link': 'http://arxiv.org/pdf/1509.09308.pdf',
                        'download_url': 'http://arxiv.org/pdf/1509.09308.pdf',
                        'abstract': "Deep convolutional neural networks take GPU days of compute time to train on\nlarge data sets. Pedestrian detection for self driving cars requires very low\nlatency. Image recognition for mobile phones is constrained by limited\nprocessing resources. The success of convolutional neural networks in these\nsituations is limited by how fast we can compute them. Conventional FFT based\nconvolution is fast for large filters, but state of the art convolutional\nneural networks use small, 3x3 filters. We introduce a new class of fast\nalgorithms for convolutional neural networks using Winograd's minimal filtering\nalgorithms. The algorithms compute minimal complexity convolution over small\ntiles, which makes them fast with small filters and small batch sizes. We\nbenchmark a GPU implementation of our algorithm with the VGG network and show\nstate of the art throughput at batch sizes from 1 to 64",  # noqa
                        'fingerprint': [
                            'Convolutional', 'neural network', 'Convolutional Neural', 'network',
                            'Neural', 'Neural Networks', 'compute', 'compute minimal',
                            'convolutional code', 'turbo code', 'turbo', 'network structure',
                            'communication network', 'calculate', 'derive', 'artificial neural',
                            'feedforward neural', 'obtain', 'algorithm', 'performance', 'image',
                            'social', 'state', 'distance', 'application', 'scheme', 'detection', 'joint'
                        ],
                        'name': 'fast_algorithms_for_convolutional_neural_oai:arXiv.org:1509.09308',
                        'metadata': {'type': 'original'},
                        'children': [],
                        'ord_id': 8
                    }],
                    'ord_id': 7
                }, {
                    'title': 'Batch Normalized Recurrent Neural Networks',
                    'year': 2015,
                    'identifier': 'oai:arXiv.org:1510.01378', 'journal': None,
                    'fulltext_link': 'http://arxiv.org/pdf/1510.01378.pdf',
                    'download_url': 'http://arxiv.org/pdf/1510.01378.pdf',
                    'abstract': "Recurrent Neural Networks (RNNs) are powerful models for sequential data that\nhave the potential to learn long-term dependencies. However, they are\ncomputationally expensive to train and difficult to parallelize. Recent work\nhas shown that normalizing intermediate representations of neural networks can\nsignificantly improve convergence rates in feedforward neural networks . In\nparticular, batch normalization, which uses mini-batch statistics to\nstandardize features, was shown to significantly reduce training time. In this\npaper, we show that applying batch normalization to the hidden-to-hidden\ntransitions of our RNNs doesn't help the training procedure. We also show that\nwhen applied to the input-to-hidden transitions, batch normalization can lead\nto a faster convergence of the training criterion but doesn't seem to improve\nthe generalization performance on both our language modelling and speech\nrecognition tasks. All in all, applying batch normalization to RNNs turns out\nto be more challenging than applying it to feedforward networks, but certain\nvariants of it can still be beneficial",  # noqa
                    'fingerprint': [
                        'RNNs', 'batch', 'batch normalization', 'normalization', 'Networks RNNs',
                        'batch Normalized', 'transition batch', 'Neural', 'Recurrent Neural',
                        'Neural Networks', 'Recurrent', 'Normalized', 'batch reactor',
                        'batch culture', 'lab-scale', 'normalisation', 'repraesentatio',
                        'post-smoothed', 'network-based recommendation', 'correction', 'summation',
                        'neural network', 'artificial neural', 'normalised', 'feedforward neural',
                        'corrected', 'calculated', 'paroxysmal', 'hiv-associated', 'biopsy-proven',
                        'state', 'social', 'growth', 'school', 'learning', 'optimal', 'day',
                        'network', 'solution', 'feature', 'community'
                    ],
                    'name': 'batch_normalized_recurrent_neural_networks_oai:arXiv.org:1510.01378',
                    'metadata': {'type': 'original'},
                    'children': [{
                        'title': 'ImageNet Large Scale Visual Recognition Challenge',
                        'year': 2015,
                        'identifier': 'oai:arXiv.org:1409.0575',
                        'journal': None,
                        'fulltext_link': 'http://arxiv.org/pdf/1409.0575.pdf', 'download_url': None,
                        'abstract': 'The ImageNet Large Scale Visual Recognition Challenge is a benchmark in\nobject category classification and detection on hundreds of object categories\nand millions of images. The challenge has been run annually from 2010 to\npresent, attracting participation from more than fifty institutions.\n  This paper describes the creation of this benchmark dataset and the advances\nin object recognition that have been possible as a result. We discuss the\nchallenges of collecting large-scale ground truth annotation, highlight key\nbreakthroughs in categorical object recognition, provide a detailed analysis of\nthe current state of the field of large-scale image classification and object\ndetection, and compare the state-of-the-art computer vision accuracy with human\naccuracy. We conclude with lessons learned in the five years of the challenge,\nand propose future directions and improvements.Comment: 43 pages, 16 figures. v3 includes additional comparisons with PASCAL\n  VOC (per-category comparisons in Table 3, distribution of localization\n  difficulty in Fig 16), a list of queries used for obtaining object detection\n  images (Appendix C), and some additional reference',  # noqa
                        'fingerprint': [
                            'ImageNet', 'ImageNet Large', 'Challenge', 'Recognition Challenge',
                            'Recognition', 'Visual Recognition', 'Visual', 'Large', 'Scale Visual',
                            'object detection', 'Large Scale', 'Scale', 'pascal voc',
                            'image classification', 'texture-based', 'detection', 'challenge',
                            'small', 'dilemma', 'formidable challenge', 'face recognition',
                            'auditory', 'speech recognition', 'stage', 'pattern recognition',
                            'perceptual', 'visual stimulus', 'scale', 'huge', 'larger',
                            'scale model', 'rating scale', 'direct detection', 'detection method',
                            'detection technique', 'human', 'object', 'survival', 'algorithm',
                            'performance', 'category', 'grade'
                        ],
                        'name': 'imagenet_large_scale_visual_recognition_oai:arXiv.org:1409.0575',
                        'metadata': {'type': 'original'},
                        'children': [],
                        'ord_id': 10
                    }],
                    'ord_id': 9
                }],
                'ord_id': 2
            }],
            'ord_id': 1
        }

    def test_tree_comparison(self):
        comparator = TreeComparator(self.original, self.review)
        self.assertTrue(comparator.check_for_differences)
