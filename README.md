# LEDGER Application

## License information
Project AIUR software in the current repository is publicly available as
free and open source code under MIT license (https://opensource.org/licenses/MIT or https://tldrlegal.com/license/mit-license)

## Linux Installation

  * Install git
```
#!bash
sudo apt-get update
sudo apt-get install git
```
  * Install ssh public key
```
#!bash
eval `ssh-agent -s`

# use already existing key
cp /path/to/my/key/id_rsa ~/.ssh/id_rsa
cp /path/to/my/key/id_rsa.pub ~/.ssh/id_rsa.pub
# change permissions on file
sudo chmod 600 ~/.ssh/id_rsa
sudo chmod 600 ~/.ssh/id_rsa.pub
ssh-add ~/.ssh/id_rsa
```
* Checkout this repo
```
#!bash
git clone git@bitbucket.org:irisai/aiur_app_git.git
```
  * Update python3
```
#!bash
sudo apt-get update
sudo apt-get install python3-pip python3-dev
```
  * Install required python libraries
```
#!bash
sudo pip3 install -r requirements.txt
```

## Running unit tests

```
#!bash
export DJANGO_SETTINGS_MODULE=aiur_django.settings
python manage.py test test/unit_tests/
```

## Running integration tests

1. Add your AWS credentials to a file with name `aws_keys.py` and place it under _test/tools/references\_pdf\_parser_
2. Run the following command

```
python -m unittest test_references_pdf_parser.py
```

The tests will first check for a folder called `test_pdfs`. If no such folder exists then the PDF files used for testing will be downloaded from s3 bucket called `iris.pdf.parser.test.pdfs` and placed in folder with name `test_pdfs`.
